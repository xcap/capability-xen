/**
 * common/xen-cap.c
 *
 * This file is a part of Flux Xen Capabilities
 *
 * Capabilities Interface for Xen
 *
 * Authors: Yathindra Naik
 * Date: September 2012
 */



#include <xen/mm.h>
#include <xen/xmalloc.h>
#include <xen/sched.h>
#include <xen/paging.h>
#include <xen/xen-cap.h>
#include "uthash.h"

/* Capability tokens are random numbers for now. This implementation is taken from wikipedia.
 * Source: http://en.wikipedia.org/wiki/Multiply-with-carry
 * This is based on Multiply-with-carry random number generation algorithm invented by George Marsaglia.
 */
struct cap_boot_info *CAP_BOOT_INFO;

uint32_t RNGQ[4096], RNG_CARRY = 362436;

void init_rand(uint32_t x)
{
    int i;

    RNGQ[0] = x;
    RNGQ[1] = x + PHI;
    RNGQ[2] = x + PHI + PHI;

    for (i = 3; i < 4096; i++)
        RNGQ[i] = RNGQ[i - 3] ^ RNGQ[i - 2] ^ PHI ^ i;
}

uint32_t rand_cmwc(void)
{
    uint64_t t, a = 18782LL;
    static uint32_t i = 4095;
    uint32_t x, r = 0xfffffffe;
    i = (i + 1) & 4095;
    t = a * RNGQ[i] + RNG_CARRY;
    RNG_CARRY = (t >> 32);
    x = t + RNG_CARRY;
    if (x < RNG_CARRY) {
        x++;
        RNG_CARRY++;
    }
    return (RNGQ[i] = r - x);
}


/*
 * This routine is used to populate the initial capabilities.
 * Dom0 by default has all the capabilities.
 * DomU has to be granted capabilities.
 */

int cap_init(void)
{
	int ret = 0;

	TT_DBG("In cap_init()\n");

	TT_DBG("	-cap_share_boot_info_page()\n");

	/* Initialize the boot info page. */
	ret = cap_share_boot_info_page();
	
	if ( ret == -1 )
		return ret; /* Failed */
	
	TT_DBG("	-cap_init_hypercalls()\n");

	/* Initialize the capabilities for hypercalls */
	ret = cap_init_hypercalls();

	/* TO-DO: Initialize other resources here with 
	 * capabilities.
	 */
	TT_DBG("Leaving cap_init()\n");
	return ret;
}

int cap_share_boot_info_page()
{
	unsigned long order, cap_boot_info_size;
	int i, ret = 0;

	TT_DBG("In cap_share_boot_info_page()\n");
	TT_DBG("	- get_order_from_bytes()\n");
	order = get_order_from_bytes(sizeof(struct cap_boot_info));
	
	TT_DBG("	- alloc_xenheap_pages()\n");

	/* Allocate page for cap_boot_info */	
	if ( (CAP_BOOT_INFO = alloc_xenheap_pages(order,0)) == NULL ) {
		TT_ERR("Capability boot info: memory allocation failed\n");
		ret = -1; goto cleanup;
	}
    
	cap_boot_info_size =  1 << order;

	TT_DBG("	- share_xen_page_with_privileged_guests()\n");

	/* Share cap_boot_info with the hypervisor */
	for ( i = 0; i < cap_boot_info_size; ++i )
	{
		 share_xen_page_with_privileged_guests(	
			 virt_to_page(CAP_BOOT_INFO) + i, XENSHARE_readonly);
	}

	TT_DBG("Leaving cap_share_boot_info_page()\n");

cleanup:
#if 0
	if ( cap_boot_info )
	{
		free_xenheap_pages((void*)cap_boot_info, order);
		cap_boot_info = NULL;
	}
#endif	
	return ret;
}

/* Intialize the capabilities for hypercalls. */
int cap_init_hypercalls()
{
	int i;
	init_rand(41);

	TT_DBG("In cap_init_hypercalls()\n");

	/* capability is a random number. */
	for ( i = 0; i < NUM_HYPERCALLS; ++i )
	{
		CAP_BOOT_INFO->cap_hypercalls[i].magic = rand_cmwc();
	}

	TT_DBG("Leaving cap_init_hypercalls()\n");
	return 0;
}

/*
 * Since I cannot pass struct domain to cap_create
 * this is a helper routine only for Domain-0
 */
int dom0_cap_create(struct domain *d, struct capability *cap)
{
	TT_DBG("\n");
	if (d->domain_id != 0)
	{
		TT_ERR("Illegal domain id\n");
		return 1;
	}

    if ( cap == NULL )
    {
        TT_ERR("cap_create: Domain: %d cap is NULL!\n",d->domain_id);
        return 1;
    }

    //TT_DBG("In cap_create()\n");
    cap->magic = rand_cmwc();

    /* NOTE: I'm not sure if we need to insert the newly
     * create cap in calling domains cap_space.
     * For now, this will help me with xenstore since
     * which ever domain is calling cap_create will have
     * the necessary capabilities in its cap_space.
     * Also, note that the *cap below might be a local
     * pointer!!!!!
     */
    d->cap_space->num_caps++;
    TT_DBG("Domain %d Capability 0x%x inserted at index %d\n",d->domain_id, cap->magic, d->cap_space->num_caps);
    return 0;
}

/* 
 * Creates a capability.
 */
int cap_create(struct capability *cap)
{
	int num_caps;
	struct domain *d = current->domain;
	init_rand(41);

	if (d->domain_id > 100) // change this to DOMID_SELF ?
	{
		if (rcu_lock_target_domain_by_id(0,&d))
		{
			TT_ERR("could not lock domain 0\n");
			return 1;
		}
	}

	if (d->cap_flag == 0)
	{
		TT_DBG("Domain %d not in capability mode!\n",current->domain->domain_id);
		return 0;
	}

	if ( d->cap_space != NULL )
		num_caps = d->cap_space->num_caps;
	else
	{	
        TT_ERR("cap_create: Domain: %d cap_space not init'ed\n",d->domain_id);
        return 1;
	}

	if ( cap == NULL )
	{
		TT_ERR("cap_create: Domain: %d cap is NULL!\n",d->domain_id);
		return 1;
	}

	//TT_DBG("In cap_create()\n");
	cap->magic = rand_cmwc();

	/* NOTE: I'm not sure if we need to insert the newly
	 * create cap in calling domains cap_space.
	 * For now, this will help me with xenstore since
	 * which ever domain is calling cap_create will have
	 * the necessary capabilities in its cap_space.
	 * Also, note that the *cap below might be a local 
	 * pointer!!!!!
	 */  

	if (d->domain_id > 0 && d->domain_id < 20)
		if (num_caps >=0 && num_caps <= 5)
			TT_ERR("Domain %d inserting into index %d Capability 0x%x\n",d->domain_id,num_caps,cap->magic);

	d->cap_space->caps[num_caps] = *cap;
	d->cap_space->num_caps++;
	TT_DBG("Domain %d Capability 0x%x inserted at index %d\n",d->domain_id, cap->magic, d->cap_space->num_caps);
	if (d->domain_id == 0)
		rcu_unlock_domain(d);
	return 0;

}

/*
 * Check if the domain has the required capability.
 */
int cap_check(struct domain *d, struct capability *cap)
{
	int i;
	int ret = 0;
	int num_caps;

	if ( IS_PRIV(d) )
		return 1;

	if ( d->domain_id < 0 || d->domain_id > 100 )
		return 1;

    if (d->cap_flag == 0)
    {
        TT_DBG("Domain %d not in capability mode!\n",d->domain_id);
        return ret;
    }

    if ( d->cap_space != NULL )
        num_caps = d->cap_space->num_caps;
    else
    {
        TT_DBG("Domain: %d cap_space not init'ed\n",d->domain_id);
        return 1;
    }
	
	/* capability is a random number. */
	for ( i = 0; i < num_caps ; ++i )
	{
		//TT_DBG("Domain %d index %d Capability 0x%x\n",d->domain_id, i, d->cap_space->caps[i].magic);
		if ( d->cap_space->caps[i].magic == cap->magic )
			return 1; /* Found */
	}	
		
	TT_ERR("Domain %d Capability 0x%x check failed!\n",d->domain_id, cap->magic);
	return ret;	/* Not found */
}


/* 
 * Grant a list of capabilities from source domain to destination domain.
 */
int cap_grant(struct domain *to, struct capability *list, int size)
{
    int i, num_caps;
	struct domain *from = current->domain;
	int ret = 0; /* success */

	if (from->domain_id < 0 || from->domain_id > 100)
		TT_ERR("current domain is %d\n",from->domain_id);

    if (to->cap_flag == 0)
    {
        TT_DBG("Domain %d not in capability mode!\n",to->domain_id);
        return 0;
    }

	if ( to->cap_space != NULL )
		num_caps = to->cap_space->num_caps;
	else
	{
		TT_DBG("Domain: %d cap_space not init'ed\n",to->domain_id);
		return 1;
	}

	//TT_DBG("In cap_grant()\n");
	
	if(from == NULL || to == NULL)
	{
		TT_DBG("Domain struct is NULL\n");
		return 1;
	}

	for ( i = 0; i < size; ++i )
	{
		if ( cap_check(from,&list[i]) )
		{
			to->cap_space->caps[num_caps].magic = list[i].magic;
	//		TT_DBG("Domain %d, index %d (to)Capability: 0x%x (list)Capability: 0x%x\n",to->domain_id, to->cap_space->num_caps, to->cap_space->caps[num_caps].magic, list[i].magic);
			to->cap_space->num_caps++;
		}
		else
		{
			TT_DBG("Granting Capability failed since cap_check failed for domain: %d\n", from->domain_id);
			ret = 1;
		}
	}

	return ret;
}

/*
 * get_caps_hypercall 
 * return the capability associated with a specific hypercall
 * NULL if there is none
 */

void get_caps_hypercall(char *tok, struct capability *cap)
{
	struct domain *from = current->domain;
	
	if (from == NULL)
	{
		TT_DBG("Domain struct is NULL\n");
		return;
	}
	if ( cap == NULL )
	{
		TT_DBG("Cap is NULL!\n");
		return;
	}
	
	if ( strncmp(tok,"memory_pin_page",strlen("memory_pin_page")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[1].magic;
	if ( strncmp(tok,"security_domaininfo",strlen("security_domaininfo")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[2].magic;
	if ( strncmp(tok,"setvcpucontext",strlen("setvcpucontext")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[3].magic;
	if ( strncmp(tok,"pausedomain",strlen("pausedomain")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[4].magic;
	if ( strncmp(tok,"unpausedomain",strlen("unpausedomain")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[5].magic;
	if ( strncmp(tok,"resumedomain",strlen("resumedomain")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[6].magic;
	if ( strncmp(tok,"max_vcpus",strlen("max_vcpus")) == 0)
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[7].magic;
	if ( strncmp(tok,"destroydomain",strlen("destroydomain")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[8].magic;
	if ( strncmp(tok,"vcpuaffinity",strlen("vcpuaffinity")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[9].magic;
	if ( strncmp(tok,"scheduler",strlen("schedule")) == 0)
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[10].magic;
	if ( strncmp(tok,"getdomaininfo",strlen("getdomaininfo")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[11].magic;
	if ( strncmp(tok,"getvcpucontext",strlen("getvcpucontext")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[12].magic;
	if ( strncmp(tok,"getvcpuinfo",strlen("getvcpuinfo")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[13].magic;
	if ( strncmp(tok,"domain_settime",strlen("domain_settime")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[14].magic;
	if ( strncmp(tok,"set_target",strlen("set_target")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[15].magic;
	if ( strncmp(tok,"domctl",strlen("domctl")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[16].magic;
	if ( strncmp(tok,"set_virq_handler",strlen("set_virq_handler")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[17].magic;
	if ( strncmp(tok,"tbufcontrol",strlen("tbufcontrol")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[18].magic;
	if ( strncmp(tok,"readconsole",strlen("readconsole")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[19].magic;
	if ( strncmp(tok,"cap_sched_id",strlen("cap_sched_id")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[20].magic;
	if ( strncmp(tok,"setdomainmaxmem",strlen("setdomainmaxmem")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[21].magic;
	if ( strncmp(tok,"setdomainhandle",strlen("setdomainhandle")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[22].magic;
	if ( strncmp(tok,"setdebugging",strlen("setdebugging")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[23].magic;
	if ( strncmp(tok,"perfcontrol",strlen("perfcontrol")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[24].magic;
	if ( strncmp(tok,"debug_keys",strlen("debug_keys")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[25].magic;
	if ( strncmp(tok,"getcpuinfo",strlen("getcpuinfo")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[26].magic;
	if ( strncmp(tok,"get_pmstat",strlen("get_pmstat")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[27].magic;
	if ( strncmp(tok,"setpminfo",strlen("setpminfo")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[28].magic;
	if ( strncmp(tok,"pm_op",strlen("pm_op")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[29].magic;
	if ( strncmp(tok,"do_mca",strlen("do_mca")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[30].magic;
	if ( strncmp(tok,"availheap",strlen("availheap")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[31].magic;
	if ( strncmp(tok,"kexec",strlen("kexec")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[33].magic;
	if ( strncmp(tok,"evtchn_close_post",strlen("evtchn_close_post")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[34].magic;
	if ( strncmp(tok,"grant_mapref",strlen("grant_mapref")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[35].magic;
	if ( strncmp(tok,"grant_unmapref",strlen("grant_unmapref")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[36].magic;
	if ( strncmp(tok,"grant_setup",strlen("grant_setup")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[37].magic;
	if ( strncmp(tok,"grant_transfer",strlen("grant_transfer")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[38].magic;
	if ( strncmp(tok,"grant_copy",strlen("grant_copy")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[39].magic;
	if ( strncmp(tok,"grant_query_size",strlen("grant_query_size")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[40].magic;
	if ( strncmp(tok,"memory_adjust_reservation",strlen("memory_adjust_reservation")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[41].magic;
	if ( strncmp(tok,"memory_stat_reservation",strlen("memory_stat_reservation")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[42].magic;
	if ( strncmp(tok,"console_io",strlen("console_io")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[43].magic;
	if ( strncmp(tok, "profile",strlen("profile")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[44].magic;
	if ( strncmp(tok,"schedop_shutdown",strlen("schedop_shutdown")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[45].magic;
	if ( strncmp(tok,"evtchn_unbound",strlen("evtchn_unbound")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[46].magic;
	if ( strncmp(tok,"evtchn_interdomain",strlen("evtchn_interdomain")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[47].magic;
	if ( strncmp(tok,"evtchn_send",strlen("evtchn_send")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[48].magic;
	if ( strncmp(tok,"evtchn_status",strlen("evtchn_status")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[49].magic;
	if ( strncmp(tok,"evtchn_reset",strlen("evtchn_reset")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[50].magic;
	if ( strncmp(tok,"alloc_security_evtchn",strlen("alloc_security_evtchn")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[51].magic;
	if ( strncmp(tok,"free_security_evtchn",strlen("free_security_evtchn")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[52].magic;
	if ( strncmp(tok,"show_security_evtchn",strlen("show_security_evtchn")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[53].magic;
	if ( strncmp(tok,"get_pod_target",strlen("get_pod_target")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[54].magic;
	if ( strncmp(tok,"set_pod_target",strlen("set_pod_target")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[55].magic;
	if ( strncmp(tok,"resource_plug_core",strlen("resource_plug_core")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[56].magic;
	if ( strncmp(tok,"resource_unplug_core",strlen("resource_unplug_core")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[57].magic;
	if ( strncmp(tok,"resource_plug_pci",strlen("resource_plug_pci")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[58].magic;
	if ( strncmp(tok,"resource_unplug_pci",strlen("resource_unplug_pci")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[59].magic;
	if ( strncmp(tok,"resource_setup_pci",strlen("resource_setup_pci")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[60].magic;
	if ( strncmp(tok,"resource_setup_gsi",strlen("resource_setup_gsi")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[61].magic;
	if ( strncmp(tok,"resource_setup_misc",strlen("resource_setup_misc")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[62].magic;
	if ( strncmp(tok,"page_offline",strlen("page_offline")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[63].magic;
	if ( strncmp(tok,"lockprof",strlen("lockprof")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[64].magic;
	if ( strncmp(tok,"cpupool_op",strlen("cpupool_op")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[65].magic;
	if ( strncmp(tok,"sched_op",strlen("sched_op")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[66].magic;
	if ( strncmp(tok,"show_irq_sid",strlen("show_irq_show")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[67].magic;
	if ( strncmp(tok,"map_domain_pirq",strlen("map_domain_pirq")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[68].magic;
	if ( strncmp(tok,"irq_permission",strlen("irq_permission")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[69].magic;
	if ( strncmp(tok,"iomem_permission",strlen("iomem_permission")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[70].magic;
	if ( strncmp(tok,"pci_config_permission",strlen("pci_config_permission")) == 0 )
		cap->magic = CAP_BOOT_INFO->cap_hypercalls[71].magic;

	TT_DBG("Hypercall: %s Capability: 0x%x addr %p\n",tok,cap->magic,(void*)cap);
}
													 
																			  
