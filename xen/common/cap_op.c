/*
 * This file contains Capability hypercall definitions.
 * 
 */

#include <xen/sched.h>
#include <xen/hypercall.h>
#include <xen/xmalloc.h>
#include <xen/guest_access.h>
#include <xen/xen-cap.h>
#include <asm/current.h>
#include <public/cap_op.h>

static DEFINE_SPINLOCK(cap_lock);

long do_cap_op(XEN_GUEST_HANDLE(xen_cap_op_t) u_cap_op)
{
    long ret = 0;
    struct xen_cap_op curop, *op = &curop;
	
//	if (op->domain < 0 || op->domain > 100)
		op->domain = current->domain->domain_id;

    if ( copy_from_guest(op, u_cap_op, 1) ){
		TT_ERR("Copy from guest failed\n");
        return -EFAULT;
    }

    if ( op->interface_version != XEN_CAP_OP_INTERFACE_VERSION )
        return -EACCES;

	spin_lock(&cap_lock);

    switch ( op->cmd )
    {
		case XEN_CAP_OP_cap_create:
		{
		 	//struct domain *d; 
			struct capability *cap;
    		TT_DBG("XEN_CAP_OP_cap_create hypercall: domid:%d Addr of Cap %p\n", op->domain, (void*)op->u.cap_create.cap);
			/*d = rcu_lock_domain_by_id(op->domain);
	
			if ( d == NULL )
			{
				TT_ERR("Failed to rcu_lock_dom_by_id:%d\n", op->domain);
				break;
			}*/
			
			cap = op->u.cap_create.cap;

			if ((ret = cap_create(cap)) == 1)
				TT_ERR("cap_create failed\n");

			if (copy_to_guest(u_cap_op, op, 1))
			{
				TT_ERR("copy_to_guest failed!\n");
				ret = -EFAULT;
			}
			//rcu_unlock_domain(d);
		}
		break;

		case XEN_CAP_OP_cap_grant:
		{
			//struct domain *d;
			struct domain *to;
			struct capability *list;
			int i;
			int size;
			
			/*d = rcu_lock_domain_by_id(op->domain);
			if ( d == NULL )
			{
				TT_ERR("Failed to rcu_lock_dom_by_id:%d\n", op->domain);
				break;
        	}*/
		
			to   = rcu_lock_domain_by_id(op->u.cap_grant.to);
			if ( to == NULL )
			{
				TT_ERR("Failed to rcu_lock_dom_by_id\n");
				break;
			}
			
			size = op->u.cap_grant.size;
			list = op->u.cap_grant.list;
			
			for (i = 0; i < size; ++i)			
				TT_DBG("XEN_CAP_OP_cap_grant: sent Capability 0x%x addr %p\n",list[i].magic, (void*)&list[i]);


			if ( (ret = cap_grant(to,list,size)) != 0 )
			{
				TT_ERR("cap_grant failed when called from xc_cap_grant().\n");
				rcu_unlock_domain(to);
				return ret;
			}

			/*
			list = xmalloc_array(struct capability, size);
			if (list == NULL)
			{
				TT_ERR("XEN_CAP_OP_cap_grant: xmalloc_array failed\n");
				return -EFAULT;
			}
	
			if ((ret = copy_from_guest(list, op->u.cap_grant.list, size)) != 0)
			{
				TT_ERR("XEN_CAP_OP_cap_grant: copy_from_guest failed, ret %ld!\n",ret);
				return -EFAULT;
			}
			TT_DBG("grant hypercall: domid:%d list(addr): %p\n", op->domain, (void*)&list);

			if ( (ret = cap_grant(to,list,size)) != 0 )
			{
				TT_ERR("XEN_CAP_OP_cap_grant: failed when called from xc_cap_grant().\n");
				rcu_unlock_domain(to);
				//rcu_unlock_domain(d);
				break;
			}
			*/
			/*
			for (i = 0; i < size; ++i)
			{
				if (copy_from_guest_offset(&list, op->u.cap_grant.list, i, 1))
				{
					TT_ERR("copy_from_user_offset failed!\n");					
					return -EFAULT;
				}
				TT_DBG("grant hypercall: domid:%d list(addr): %p\n", op->domain, (void*)&list);

				if ( (ret = cap_grant(to,&list,1)) != 0 )
				{
					TT_ERR("cap_grant failed when called from xc_cap_grant().\n");
					rcu_unlock_domain(to);
					//rcu_unlock_domain(d);
					break;
				}

			}
			*/
			//rcu_unlock_domain(d);
			//xfree(list);
			rcu_unlock_domain(to);
		}
		break;

		case XEN_CAP_OP_cap_check:
		{
			struct domain *d;
			struct capability *cap;

			d = rcu_lock_domain_by_id(op->domain);
			if ( d == NULL )
			{
				TT_ERR("Failed to rcu_lock_dom_by_id:%d\n", op->domain);
				break;
			}
			
			cap  = op->u.cap_check.cap;

			ret = cap_check(d,cap);		
			rcu_unlock_domain(d);
		}
		break;

		case XEN_CAP_OP_get_caps_hypercall:
		{
			struct capability *cap;
			char *tok;
			//struct domain *d;

			//d = rcu_lock_domain_by_id(op->domain);
			
            cap = op->u.get_caps_hypercall.cap;
			tok = op->u.get_caps_hypercall.tok;
			TT_DBG("XEN_CAP_OP_get_caps_hypercalls: sent Capability 0x%x addr %p\n",cap->magic, (void*)&cap);
		/*	
			if (copy_from_guest(&cap, op->u.get_caps_hypercall.cap, 1))
			{
				TT_ERR("copy_from_guest failed!\n");
				return -EFAULT;
			}
		*/
            get_caps_hypercall(tok,cap);             

			TT_DBG("XEN_CAP_OP_get_caps_hypercalls: return Capability 0x%x addr %p\n",op->u.get_caps_hypercall.cap->magic, (void*)op->u.get_caps_hypercall.cap);
			if (copy_to_guest(u_cap_op, op, 1))
			{	
				TT_ERR("XEN_CAP_OP_get_caps_hypercall: copy_to_guest failed!\n");
				ret = -EFAULT;
			}
			//rcu_unlock_domain(d);
		}
		break;	

    }

    spin_unlock(&cap_lock);

    return ret;
}


