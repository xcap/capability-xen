/*
 * This file contains the Xen-Cap hook functions implementations for Xen.
 * 
 * Author: Yathindra Naik <ydev@cs.utah.edu>
 *
 */

#include <xen/init.h>
#include <xen/lib.h>
#include <public/xen.h>
#include <public/platform.h>
#include <public/physdev.h>
#include <xen/xen-cap.h>
#include <asm/msi.h>
#include <xen/cpumask.h>
#include <xsm/xsm.h>
#include <xen/sched.h>

struct xsm_operations *original_ops = NULL;

CAP_T HYPERCALL_BLOCK;

static int cap_domain_create(struct domain *d, u32 ssidref)
{
	printk("Mediating cap_domain_create\n");
	return xen_cap_check(d, HYPERCALL_BLOCK);
}


static void cap_security_domaininfo(struct domain *d,
                                    struct xen_domctl_getdomaininfo *info)
{
	int ret;
    printk("Mediating cap_security_domaininfo\n");
    ret = xen_cap_check(d, HYPERCALL_BLOCK);
	return;
}

static int cap_setvcpucontext(struct domain *d)
{
	printk("Mediating cap_setvcpucontext\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_pausedomain (struct domain *d)
{
	printk("Mediating cap_pausedomain\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_unpausedomain (struct domain *d)
{
	printk("Mediating cap_unpausedomain\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_resumedomain (struct domain *d)
{
	printk("Mediating cap_resumedomain\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_max_vcpus(struct domain *d)
{
	printk("Mediating cap_max_vcpus\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_destroydomain (struct domain *d)
{
	printk("Mediating cap_destroydomain\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_vcpuaffinity (int cmd, struct domain *d)
{
	printk("Mediating cap_vcpuaffinity\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_scheduler (struct domain *d)
{
	printk("Mediating cap_scheduler\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_getdomaininfo (struct domain *d)
{
    if ( !IS_PRIV(d) )
        return -EPERM;
	printk("Mediating cap_getdomaininfo\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_getvcpucontext (struct domain *d)
{
	printk("Mediating cap_getvcpucontext\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_getvcpuinfo (struct domain *d)
{
	printk("Mediating cap_getvcpuinfo\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_domain_settime (struct domain *d)
{
	printk("Mediating cap_domain_settime\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_set_target (struct domain *d, struct domain *e)
{
	printk("Mediating cap_set_target\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}


static int cap_domctl(struct domain *d, int cmd)
{
	printk("Mediating cap_domctl\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_set_virq_handler(struct domain *d, uint32_t virq)
{
	printk("Mediating cap_set_virq_handler\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_tbufcontrol (void)
{
	//printk("Mediating cap_tbufcontrol\n");
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_readconsole (uint32_t clear)
{
	//printk("Mediating cap_readconsole\n");
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_sched_id (void)
{
	return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_setdomainmaxmem (struct domain *d)
{
	printk("Mediating cap_setdomainmaxmem\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_setdomainhandle (struct domain *d)
{
	printk("Mediating cap_setdomainhandle\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_setdebugging (struct domain *d)
{
	printk("Mediating cap_setdebugging\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_perfcontrol (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_debug_keys (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_getcpuinfo (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_get_pmstat (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_setpminfo (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_pm_op (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_do_mca (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_availheap (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_alloc_security_domain (struct domain *d)
{
	printk("Mediating cap_alloc_security_domain\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static void cap_free_security_domain (struct domain *d)
{
	int ret;
	printk("Mediating cap_free_security_domain\n");
	ret = xen_cap_check(d, HYPERCALL_BLOCK);
    return;
}

static int cap_grant_mapref (struct domain *d1, struct domain *d2,
                                                                uint32_t flags)
{
	printk("Mediating cap_grant_mapref\n");
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

static int cap_grant_unmapref (struct domain *d1, struct domain *d2)
{
	printk("Mediating cap_grant_unmapref\n");
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

static int cap_grant_setup (struct domain *d1, struct domain *d2)
{
	printk("Mediating cap_grant_setup\n");
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

static int cap_grant_transfer (struct domain *d1, struct domain *d2)
{
	printk("Mediating cap_grant_transfer\n");
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

static int cap_grant_copy (struct domain *d1, struct domain *d2)
{
	printk("Mediating cap_grant_copy\n");
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

static int cap_grant_query_size (struct domain *d1, struct domain *d2)
{
	printk("Mediating cap_grant_query_size\n");
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

#if 0
static int cap_memory_adjust_reservation (struct domain *d1,
                                                            struct domain *d2)
{
	printk("Mediating cap_memory_adjust_reservation\n");
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

static int cap_memory_stat_reservation (struct domain *d1, struct domain *d2)
{
	printk("Mediating cap_memory_stat_reservation\n");
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

static int cap_console_io (struct domain *d, int cmd)
{
	printk("Mediating cap_console_io\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_profile (struct domain *d, int op)
{
	printk("Mediating cap_profile\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}
#endif
static int cap_kexec (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_schedop_shutdown (struct domain *d1, struct domain *d2)
{
	printk("Mediating cap_schedop_shutdown\n");
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

static int cap_memory_pin_page(struct domain *d1, struct domain *d2, struct page_info *page)
{
	printk("Mediating cap_memory_pin_page\n");
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

static int cap_evtchn_unbound (struct domain *d, struct evtchn *chn,
                                                                    domid_t id2)
{
	printk("Mediating cap_evtchn_unbound\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_evtchn_interdomain (struct domain *d1, struct evtchn
                                *chan1, struct domain *d2, struct evtchn *chan2)
{
	printk("Mediating cap_evtchn_interdomain\n");
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

static void cap_evtchn_close_post (struct evtchn *chn)
{
    return;
}

static int cap_evtchn_send (struct domain *d, struct evtchn *chn)
{
	printk("Mediating cap_evtchn_send\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_evtchn_status (struct domain *d, struct evtchn *chn)
{
	printk("Mediating cap_evtchn_status\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_evtchn_reset (struct domain *d1, struct domain *d2)
{
	printk("Mediating cap_evtchn_reset\n");
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

static int cap_alloc_security_evtchn (struct evtchn *chn)
{
	//printk("Mediating cap_alloc_security_evtchn\n");
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static void cap_free_security_evtchn (struct evtchn *chn)
{
    return;
}

static char *cap_show_security_evtchn (struct domain *d, const struct evtchn *chn)
{
    return NULL; 
}

static int cap_get_pod_target(struct domain *d)
{
	printk("Mediating cap_get_pod_target\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_set_pod_target(struct domain *d)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

#if 0
static int cap_get_device_group (uint32_t machine_bdf)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_test_assign_device (uint32_t machine_bdf)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_assign_device (struct domain *d, uint32_t machine_bdf)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_deassign_device (struct domain *d, uint32_t machine_bdf)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_resource_plug_core (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_resource_unplug_core (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_resource_plug_pci (uint32_t machine_bdf)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_resource_unplug_pci (uint32_t machine_bdf)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_resource_setup_pci (uint32_t machine_bdf)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_resource_setup_gsi (int gsi)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_resource_setup_misc (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_page_offline (uint32_t cmd)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_lockprof (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_cpupool_op (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_sched_op (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static char *cap_show_irq_sid (int irq)
{
    return NULL;
}
#endif
#if 0
static int cap_map_domain_pirq (struct domain *d, int irq, void *data)
{
	printk("Mediating cap_map_domain_pirq\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_irq_permission (struct domain *d, int pirq, uint8_t allow)
{
	printk("Mediating cap_irq_permission\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_iomem_permission (struct domain *d, uint64_t s, uint64_t e, uint8_t allow)
{
	printk("Mediating cap_iomem_permission\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_pci_config_permission (struct domain *d, uint32_t machine_bdf,
                                        uint16_t start, uint16_t end,
                                        uint8_t access)
{
	printk("Mediating cap_pci_config_permission\n");
    return xen_cap_check(d, HYPERCALL_BLOCK);
}
#endif

#ifdef CONFIG_X86
#if 0
static int cap_shadow_control (struct domain *d, uint32_t op)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_getpageframeinfo (struct domain *d)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_getmemlist (struct domain *d)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_hypercall_init (struct domain *d)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_hvmcontext (struct domain *d, uint32_t cmd)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_address_size (struct domain *d, uint32_t cmd)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_hvm_param (struct domain *d, unsigned long op)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_hvm_set_pci_intx_level (struct domain *d)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_hvm_set_isa_irq_level (struct domain *d)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_hvm_set_pci_link_route (struct domain *d)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_mem_event (struct domain *d)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_mem_sharing (struct domain *d)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_apic (struct domain *d, int cmd)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_xen_settime (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_memtype (uint32_t access)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_microcode (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_physinfo (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_platform_quirk (uint32_t quirk)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_firmware_info (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_efi_call(void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_acpi_sleep (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_change_freq (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_getidletime (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_machine_memory_map (void)
{
    return 0;// xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_domain_memory_map (struct domain *d)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_mmu_normal_update (struct domain *d, struct domain *t,
                                    struct domain *f, intpte_t fpte)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_mmu_machphys_update (struct domain *d, struct domain *f, unsigned long mfn)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_update_va_mapping (struct domain *d, struct domain *f, 
                                                            l1_pgentry_t pte)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_add_to_physmap (struct domain *d1, struct domain *d2)
{
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

static int cap_remove_from_physmap (struct domain *d1, struct domain *d2)
{
    return xen_cap_check(d1, HYPERCALL_BLOCK);
}

static int cap_sendtrigger (struct domain *d)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_bind_pt_irq (struct domain *d, struct xen_domctl_bind_pt_irq *bind)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_unbind_pt_irq (struct domain *d)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_pin_mem_cacheattr (struct domain *d)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_ext_vcpucontext (struct domain *d, uint32_t cmd)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_vcpuextstate (struct domain *d, uint32_t cmd)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}

static int cap_ioport_permission (struct domain *d, uint32_t s, uint32_t e, uint8_t allow)
{
    return xen_cap_check(d, HYPERCALL_BLOCK);
}
#endif
#endif
//long do_cap_op(XEN_GUEST_HANDLE(xsm_op_t) u_cap_op);

static struct xsm_operations cap_ops = {
    .security_domaininfo = cap_security_domaininfo,
    .setvcpucontext = cap_setvcpucontext,
    .pausedomain = cap_pausedomain,
    .unpausedomain = cap_unpausedomain,    
    .resumedomain = cap_resumedomain,    
    .domain_create = cap_domain_create,
    .max_vcpus = cap_max_vcpus,
    .destroydomain = cap_destroydomain,
    .vcpuaffinity = cap_vcpuaffinity,
    .scheduler = cap_scheduler,
    .getdomaininfo = cap_getdomaininfo,
    .getvcpucontext = cap_getvcpucontext,
    .getvcpuinfo = cap_getvcpuinfo,
    .domain_settime = cap_domain_settime,
    .set_target = cap_set_target,
    .domctl = cap_domctl,
    .set_virq_handler = cap_set_virq_handler,
    .tbufcontrol = cap_tbufcontrol,
    .readconsole = cap_readconsole,
    .sched_id = cap_sched_id,
    .setdomainmaxmem = cap_setdomainmaxmem,
    .setdomainhandle = cap_setdomainhandle,
    .setdebugging = cap_setdebugging,
    .perfcontrol = cap_perfcontrol,
    .debug_keys = cap_debug_keys,
    .getcpuinfo = cap_getcpuinfo,
    .availheap = cap_availheap,
    .get_pmstat = cap_get_pmstat,
    .setpminfo = cap_setpminfo,
    .pm_op = cap_pm_op,
    .do_mca = cap_do_mca,

    .evtchn_unbound = cap_evtchn_unbound,
    .evtchn_interdomain = cap_evtchn_interdomain,
    .evtchn_close_post = cap_evtchn_close_post,
    .evtchn_send = cap_evtchn_send,
    .evtchn_status = cap_evtchn_status,
    .evtchn_reset = cap_evtchn_reset,

    .grant_mapref = cap_grant_mapref,
    .grant_unmapref = cap_grant_unmapref,
    .grant_setup = cap_grant_setup,
    .grant_transfer = cap_grant_transfer,
    .grant_copy = cap_grant_copy,
    .grant_query_size = cap_grant_query_size,

    .alloc_security_domain = cap_alloc_security_domain,
    .free_security_domain = cap_free_security_domain,
    .alloc_security_evtchn = cap_alloc_security_evtchn,
    .free_security_evtchn = cap_free_security_evtchn,
    .show_security_evtchn = cap_show_security_evtchn,

    .get_pod_target = cap_get_pod_target,
    .set_pod_target = cap_set_pod_target,
//    .memory_adjust_reservation = cap_memory_adjust_reservation,
//    .memory_stat_reservation = cap_memory_stat_reservation,
    .memory_pin_page = cap_memory_pin_page,

//    .console_io = cap_console_io,

//    .profile = cap_profile,

    .kexec = cap_kexec,
    .schedop_shutdown = cap_schedop_shutdown,
#if 0
    .show_irq_sid = cap_show_irq_sid,
    .map_domain_pirq = cap_map_domain_pirq,
    .irq_permission = cap_irq_permission,
    .iomem_permission = cap_iomem_permission,
    .pci_config_permission = cap_pci_config_permission,
    .resource_plug_core = cap_resource_plug_core,
    .resource_unplug_core = cap_resource_unplug_core,
    .resource_plug_pci = cap_resource_plug_pci,
    .resource_unplug_pci = cap_resource_unplug_pci,
    .resource_setup_pci = cap_resource_setup_pci,
    .resource_setup_gsi = cap_resource_setup_gsi,
    .resource_setup_misc = cap_resource_setup_misc,

    .page_offline = cap_page_offline,
    .lockprof = cap_lockprof,
    .cpupool_op = cap_cpupool_op,
    .sched_op = cap_sched_op,
#endif
    //.__do_xsm_op = do_cap_op,

#ifdef CONFIG_X86
#if 0
    .shadow_control = cap_shadow_control,
    .getpageframeinfo = cap_getpageframeinfo,
    .getmemlist = cap_getmemlist,
    .hypercall_init = cap_hypercall_init,
    .hvmcontext = cap_hvmcontext,
    .address_size = cap_address_size,
    .hvm_param = cap_hvm_param,
    .hvm_set_pci_intx_level = cap_hvm_set_pci_intx_level,
    .hvm_set_isa_irq_level = cap_hvm_set_isa_irq_level,
    .hvm_set_pci_link_route = cap_hvm_set_pci_link_route,
    .mem_event = cap_mem_event,
    .mem_sharing = cap_mem_sharing,
    .apic = cap_apic,
    .xen_settime = cap_xen_settime,
    .memtype = cap_memtype,
    .microcode = cap_microcode,
    .physinfo = cap_physinfo,
    .platform_quirk = cap_platform_quirk,
    .firmware_info = cap_firmware_info,
    .efi_call = cap_efi_call,
    .acpi_sleep = cap_acpi_sleep,
    .change_freq = cap_change_freq,
    .getidletime = cap_getidletime,
    .machine_memory_map = cap_machine_memory_map,
    .domain_memory_map = cap_domain_memory_map,
    .mmu_normal_update = cap_mmu_normal_update,
    .mmu_machphys_update = cap_mmu_machphys_update,
    .update_va_mapping = cap_update_va_mapping,
    .add_to_physmap = cap_add_to_physmap,
    .remove_from_physmap = cap_remove_from_physmap,
    .sendtrigger = cap_sendtrigger,
    .get_device_group = cap_get_device_group,
    .test_assign_device = cap_test_assign_device,
    .assign_device = cap_assign_device,
    .deassign_device = cap_deassign_device,
    .bind_pt_irq = cap_bind_pt_irq,
    .unbind_pt_irq = cap_unbind_pt_irq,
    .pin_mem_cacheattr = cap_pin_mem_cacheattr,
    .ext_vcpucontext = cap_ext_vcpucontext,
    .vcpuextstate = cap_vcpuextstate,
    .ioport_permission = cap_ioport_permission,
#endif
#endif
};

static __init int cap_init(void)
{
	int ret = 0;
 	CAP_T HYPERCALL_BLOCK;

	CAP_DEFINE(HYPERCALL_BLOCK, 0xcafebabe, 0x12314123, 0xdefaddef, 0x123defed);

	printk("Xen-cap: Initializing and registering with XSM.\n");

	original_ops = xsm_ops;

	if ( register_xsm(&cap_ops) )
		printk("xen-cap: Unable to register with XSM.\n");

	printk("Leaving cap_init.\n");
	return ret;
}

xsm_initcall(cap_init);
	
