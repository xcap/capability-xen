/*
 * This file contains the Xen-Cap hook functions implementations for Xen.
 *
 * Author: Yathindra Naik <ydev@cs.utah.edu>
 *
 */

#include <xen/init.h>
#include <xen/lib.h>
#include <public/xen.h>
#include <public/platform.h>
#include <public/physdev.h>
#include <asm/msi.h>
#include <xen/cpumask.h>
#include <xen/sched.h>
#include <xsm/xsm.h>
#include <xen/cpumask.h>
#include <xen/errno.h>
#include <xen/guest_access.h>
#include <xen/xenoprof.h>
#include <asm/msi.h>
#include <public/xen.h>
#include <public/physdev.h>
#include <public/platform.h>
#include <xen/xmalloc.h>
#include <xen/xen-cap.h>

struct xsm_operations *original_ops = NULL;

int cap_debug = 1;

static int cap_domain_create(struct domain *d, u32 ssidref)
{
	int rc = 0;
	struct cap_space *dom_cap_space  = xmalloc(struct cap_space);
	int i;

	if ( dom_cap_space == NULL )
	{
		TT_DBG_ON(cap_debug,"dom_cap_space allocation failed in cap_domain_create()!\n");
		return -1;
	}

	/* If this is not in capability mode, Simply return now. 
	   For now, domain 0 is by default in capability mode.
	*/
	if ( d->domain_id != 0 && d->cap_flag == 0 )
	{
		TT_DBG_ON(cap_debug,"Domain is not in capability mode. Caps not alloc'ed. Returning.\n");
		xfree(dom_cap_space);
		return rc;
	}

	dom_cap_space->caps				= xmalloc_array(struct capability, NUM_CAPS);
	dom_cap_space->num_caps			= 0;

	if ( dom_cap_space->caps == NULL )
	{
		xfree(dom_cap_space);
		TT_DBG_ON(cap_debug,"dom_cap_space->caps allocation failed in cap_domain_create()!\n");
		return -1;
	}

	TT_DBG_ON(cap_debug,"In cap_domain_create\n");

		
	/* Dom0 is all-powerful. We'll change this later.
	 * So in order to populate the capabilities, we'll
	 * simply point the dom0's cap_space pointer to the
	 * CAP_BOOT_INFO's capability list for various resources.
	 */
	
	/* Hack: IS_PRIV don't recognize domain 0 as priv ; so 
	 * using domid instead.
	 */

    if ( d->domain_id == 0 )
    {
		TT_DBG_ON(cap_debug,"Init'ing capabilities for domain %d\n",d->domain_id);
		for (i = 0;i < NUM_HYPERCALLS; ++i)
		{
			dom_cap_space->caps[i] =  CAP_BOOT_INFO->cap_hypercalls[i];			
			dom_cap_space->num_caps++;
		}
		d->cap_space   = dom_cap_space;
		d->cap_flag    = 1;
		/* Also, init est_evtchn - allowing other domains to est event channels with domain 0 */
		d->est_evtchn = xmalloc(struct capability);
		if (d->est_evtchn == NULL)
		{
			TT_ERR("Could not alloc est_evtchn to Domain 0\n");
			return -1;
		}
		if (dom0_cap_create(d,d->est_evtchn))
		{
			TT_ERR("Could not create cap for Dom0's est_evtchn\n");
			return -1;
		}
        return rc;
    }
	else
	{
		TT_DBG_ON(cap_debug,"Init'ing cap_space for guest domain: %d\n",d->domain_id);
		d->cap_space = dom_cap_space;
	}

	TT_DBG_ON(cap_debug,"Returning from cap_domain_create\n");
    return rc;
		
}

#if 0
/* Note: In do_console_io (xen/drivers/char/console.c)
 * return val of 1 from here indicates failure.  
 */
static int cap_console_io (struct domain *d, int cmd)
{
	int ret = 0;

	struct capability *cap;

	/* domain d is asking for console IO. 
	 * Check if d has capabilities for 
	 * console access  
	 */

	TT_DBG_ON(cap_debug,"In cap_console_io()\n");
	cap = &(CAP_BOOT_INFO->cap_hypercalls[1]);

	if ( (ret =  cap_check(current->domain, cap)) == 1 )
	{
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_console_io()\n");
		return 0;
	}
	else
	{
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_console_io()\n");
		return 1;  
	}

	return ret;
}

static int cap_domain_memory_map(struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    TT_DBG_ON(cap_debug,"In cap_domain_memory_map()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[0]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_domain_memory_map()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\ncap_domain_memory_map()\n");
        return 1;
    }

    return ret;
}
#endif

static int cap_memory_pin_page(struct domain *d1, struct domain *d2, struct page_info *page)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d1->cap_flag == 0 || d2->cap_flag == 0 )
        return 0;

	//TT_DBG_ON(cap_debug,"In cap_memory_pin_page()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[1]);

    if ( (ret = cap_check(current->domain,cap)) == 1 )
    {
		//TT_DBG("cap_check success\n");
        return 0;		
    }
    else
    {
		TT_DBG("memory_pin_page hypercall cap_check failed)\n");
        return 1;
    }

    return ret;	
}

static void cap_security_domaininfo(struct domain *d,
                                    struct xen_domctl_getdomaininfo *info)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return;
    if ( current->domain->domain_id > 100 )
        return;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return;

	TT_DBG_ON(cap_debug,"In cap_security_domaininfo()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[2]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_security_domaininfo()\n");
        return;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_security_domaininfo()\n");
        return;
    }

    return;
}

static int cap_setvcpucontext(struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_setvcpucontext()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[3]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_setvcpucontext()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_setvcpucontext()\n");
        return 1;
    }

    return ret;
}

static int cap_pausedomain (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;
    
	TT_DBG_ON(cap_debug,"In cap_pausedomain()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[4]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_pausedomain()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_pausedomain()\n");
        return 1;
    }

    return ret;
}


static int cap_unpausedomain (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_unpausedomain()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[5]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_unpausedomain()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_unpausedomain()\n");
        return 1;
    }

    return ret;
}

static int cap_resumedomain (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_resumedomain()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[6]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_resumedomain()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_resumedomain()\n");
        return 1;
    }

    return ret;
}

static int cap_max_vcpus(struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_max_vcpus()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[7]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_max_vcpus()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_max_vcpus()\n");
        return 1;
    }

    return ret;
}

static int cap_destroydomain (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;
    
	TT_DBG_ON(cap_debug,"In cap_destorydomain()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[8]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_destroydomain()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_destroydomain()\n");
        return 1;
    }

    return ret;
}

static int cap_vcpuaffinity (int cmd, struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_vcpuaffinity()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[9]);
    
	if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_vcpuaffinity()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_vcpuaffinity()\n");
        return 1;
    }

    return ret;
}

static int cap_scheduler (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_scheduler()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[10]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_scheduler()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_scheduler()\n");
        return 1;
    }

    return ret;
}

static int cap_getdomaininfo (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_getdomaininfo()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[11]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_getdomaininfo()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_getdomaininfo()\n");
        return 1;
    }

    return ret;
}

static int cap_getvcpucontext (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_getvcpucontext()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[12]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_getvcpucontext()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_getvcpucontext()\n");
        return 1;
    }

    return ret;
}

static int cap_getvcpuinfo (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_getvcpuinfo()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[13]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_getvcpuinfo()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_getvcpuinfo()\n");
        return 1;
    }

    return ret;
}

static int cap_domain_settime (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_domain_settime()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[14]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_settime()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_settime()\n");
        return 1;
    }

    return ret;
}

static int cap_set_target (struct domain *d, struct domain *e)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_set_target()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[15]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_set_target()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_set_target)\n");
        return 1;
    }

    return ret;
}

static int cap_domctl(struct domain *d, int cmd)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_domctl()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[16]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_domctl()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_domctl()\n");
        return 1;
    }

    return ret;
}

static int cap_set_virq_handler(struct domain *d, uint32_t virq)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;
    
	TT_DBG_ON(cap_debug,"In cap_set_virq_handler()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[17]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_set_virq_handler()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_set_virq_handler()\n");
        return 1;
    }

    return ret;
}

static int cap_tbufcontrol (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_tbufcontrol()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[18]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_tbufcontrol()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_tbufcontrol()\n");
        return 1;
    }

    return ret;
}

static int cap_readconsole (uint32_t clear)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_readconsole()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[19]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_readconsole()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_readconsole()\n");
        return 1;
    }

    return ret;
}

static int cap_sched_id (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_sched_id()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[20]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_sched_id()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_sched_id()\n");
        return 1;
    }

    return ret;
}

static int cap_setdomainmaxmem (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_setdomainmaxmem()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[21]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_setdomainmaxmem()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_setdomainmaxmem()\n");
        return 1;
    }

    return ret;
}

static int cap_setdomainhandle (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;
    
	TT_DBG_ON(cap_debug,"In cap_setdomainhandle\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[22]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_setdomainhandle()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_setdomainhandle()\n");
        return 1;
    }

    return ret;
}

static int cap_setdebugging (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;
    
	TT_DBG_ON(cap_debug,"In cap_setdebugging()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[23]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_setdebugging()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_setdebugging()\n");
        return 1;
    }

    return ret;
}

static int cap_perfcontrol (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_perfcontrol()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[24]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_perfcontrol()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_perfcontrol()\n");
        return 1;
    }

    return ret;
}

static int cap_debug_keys (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_debug_keys()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[25]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_debug_keys()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_debug_keys()\n");
        return 1;
    }

    return ret;
}

static int cap_getcpuinfo (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_getcpuinfo()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[26]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_getcpuinfo()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_getcpuinfo()\n");
        return 1;
    }

    return ret;
}

static int cap_get_pmstat (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_get_pmstat()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[27]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_get_pmstat)\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_get_pmstat()\n");
        return 1;
    }

    return ret;
}

static int cap_setpminfo (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_setpminfo()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[28]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_setpminfo()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_setpminfo()\n");
        return 1;
    }

    return ret;
}

static int cap_pm_op (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_pm_op()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[29]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_pm_op()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_pm_op()\n");
        return 1;
    }

    return ret;
}

static int cap_do_mca (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_do_mca()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[30]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_do_mca()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_do_mca()\n");
        return 1;
    }

    return ret;
}

static int cap_availheap (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_availheap()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[31]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_availheap()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_availheap()\n");
        return 1;
    }

    return ret;
}

#if 0
static int cap_domain_alloc_security (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    
        TT_DBG_ON(cap_debug,"In cap_domain_alloc_security()\n");
    else
        return ret;

    cap = &(CAP_BOOT_INFO->cap_hypercalls[33]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        
            TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_domain_alloc_security()\n");
        
        return 0;
    }
    else
    {
        
            TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_domain_alloc_securtiy()\n");
        
        return 1;
    }

    return ret;
}

static void cap_domain_free_security (struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    
        TT_DBG_ON(cap_debug,"In cap_domain_free_security()\n");
    else
        return;

    cap = &(CAP_BOOT_INFO->cap_hypercalls[34]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        
            TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_domain_free_security()\n");
        
        return;
    }
    else
    {
        
            TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_domain_free_security)\n");
        
        return;
    }

    return;
}
#endif

static int cap_kexec (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_kexec()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[33]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_kexec()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_kexec()\n");
        return 1;
    }

    return ret;
}


static void cap_evtchn_close_post (struct evtchn *chn)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return;
    if( current->domain->domain_id > 100 )
        return;
    if ( current->domain->cap_flag == 0 )
        return;

    TT_DBG_ON(cap_debug,"In cap_evtchn_close_post()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[34]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_evtchn_close_post()\n");
        return;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_evtchn_close_post()\n");
        return;
    }

    return;
}


static int cap_grant_mapref (struct domain *d1, struct domain *d2,
                                                                uint32_t flags)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d1->cap_flag == 0 || d2->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_grant_mapref()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[35]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_grant_mapref()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_grant_mapref()\n");
        return 1;
    }

    return ret;
}

static int cap_grant_unmapref (struct domain *d1, struct domain *d2)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d1->cap_flag == 0 || d2->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_grant_unmapref()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[36]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_grant_unmapref()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_grant_unmapref()\n");
        return 1;
    }

    return ret;
}

static int cap_grant_setup (struct domain *d1, struct domain *d2)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d1->cap_flag == 0 || d2->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_grant_setup()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[37]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_grant_setup()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_grant_setup()\n");
        return 1;
    }

    return ret;
}

static int cap_grant_transfer (struct domain *d1, struct domain *d2)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d1->cap_flag == 0 || d2->cap_flag == 0 )
        return 0;	

	TT_DBG_ON(cap_debug,"In cap_grant_transfer()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[38]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_grant_transfer()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_grant_transfer()\n");
        return 1;
    }

    return ret;
}

static int cap_grant_copy (struct domain *d1, struct domain *d2)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d1->cap_flag == 0 || d2->cap_flag == 0 )
        return 0;
    
	TT_DBG_ON(cap_debug,"In cap_grant_copy()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[39]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_grant_copy()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_grant_copy()\n");
        return 1;
    }

    return ret;
}

static int cap_grant_query_size (struct domain *d1, struct domain *d2)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d1->cap_flag == 0 || d2->cap_flag == 0 )
        return 0;
    
	TT_DBG_ON(cap_debug,"In cap_grant_query_size()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[40]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_grant_query_size()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_grant_query_size()\n");
        return 1;
    }

    return ret;
}

static int cap_memory_adjust_reservation (struct domain *d1,
                                                            struct domain *d2)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d1->cap_flag == 0 || d2->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_memory_adjust_reservation()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[41]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_memory_adjust_reservation()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_memory_adjust_reservation()\n");
        return 1;
    }

    return ret;

}

static int cap_memory_stat_reservation (struct domain *d1, struct domain *d2)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d1->cap_flag == 0 || d2->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_memory_stat_reservation()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[42]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_memory_stat_reservation)\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_memory_stat_reservation()\n");
        return 1;
    }

    return ret;

}

static int cap_console_io (struct domain *d, int cmd)
{
    int ret = 0;

    struct capability *cap;

	/* Note: Console is flooded if I do anything here. 
		I'm simply returning hence. */
	return 0;

	TT_DBG_ON(cap_debug,"In cap_console_io()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[43]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_console_io()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_console_io()\n");
        return 1;
    }

    return ret;
}

static int cap_profile (struct domain *d, int op)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_profile()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[44]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_profile()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_profile()\n");
        return 1;
    }

    return ret;
}

static int cap_schedop_shutdown (struct domain *d1, struct domain *d2)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d1->cap_flag == 0 || d2->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_schedop_shutdown()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[45]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_schedop_shutdown()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_schedop_shutdown()\n");
        return 1;
    }

    return ret;

}

static int cap_evtchn_unbound (struct domain *d, struct evtchn *chn,
                                                                    domid_t id2)
{
    int ret = 0;
    struct capability *cap;
	struct domain *remote_domain;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
	{
		TT_DBG("returning\n");
        return 0;
	}

	if (DOMID_SELF == id2)
	{
		TT_DBG("Remote is Self domain\n");
		return 0;
	}
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG("\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[46]);

	/* Are we allowed to make this hypercall? */
    if ( (ret = cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG("cap_check success\n");
		/* Check if the remote domain has the cap to
		   est evtchn. Note, remote domain must exist
		   for this to work. */

		/* if the remote domain does not have caps 
		   set then dont do anything. Simply return. */
		if (d->est_evtchn == NULL)
		{
			TT_ERR("remote domain %d does not have est_evtchn\n",id2);
			return 0;
		}
		if ((remote_domain = get_domain_by_id(id2)) == NULL)
		{
			TT_ERR("could not get remote domain %d\n",id2);
			return 1;
		}
		if (cap_check(remote_domain, d->est_evtchn) == 0)
		{
			TT_ERR("remote domain %d does not have the cap to est evtchn\n",id2);
			return 1;
		}
        return 0; 
    }
    else
    {
		TT_ERR("cap_check failed!\nLeaving cap_evtchn_unbound()\n");
        return 1;
    }

    return ret;
}

static int cap_evtchn_interdomain (struct domain *d1, struct evtchn
                                *chan1, struct domain *d2, struct evtchn *chan2)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d1->cap_flag == 0 || d2->cap_flag == 0 )
        return 0;

	//TT_DBG_ON(cap_debug,"In cap_evtchn_interdomain()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[47]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		//TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_evtchn_interdomain()\n");

		/* Check if this domain (d1) has the capability
		   to bind with the remote domain (d2) */

		/* if the remote domain does not have est_evtchn
		   set then simply return. */
		if (d2->est_evtchn == NULL)
		{
			//TT_ERR("remote domain %d est_evtchn is NULL\n",d2->domain_id);
			return 0;
		}
		if (cap_check(d1, d2->est_evtchn) == 0)
		{
			//TT_ERR("Not allowed to bind with remote domain %d\n",d2->domain_id);
			return 1;
		}
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_evtchn_interdomain()\n");
        return 1;
    }

    return ret;
}

static int cap_evtchn_send (struct domain *d, struct evtchn *chn)
{
    int ret = 0;
	struct domain *remote_domain = chn->u.interdomain.remote_dom;
    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	//TT_DBG_ON(cap_debug,"In cap_evtchn_send()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[48]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		//TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_evtchn_send()\n");
		if (chn->state == ECS_INTERDOMAIN)
		{
			if (remote_domain->est_evtchn == NULL)
			{
				TT_ERR("remoted domain %d est_evtchn is NULL\n",remote_domain->domain_id);
				return 0;
			}

			if (cap_check(d,remote_domain->est_evtchn) == 0)
			{
				TT_ERR("Not allowed to send evt to remote domain\n");
				return 1;
			}
		}
        return 0;
    }
    else
    {
		//TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_evtchn_send()\n");
        return 1;
    }

    return ret;
}

static int cap_evtchn_status (struct domain *d, struct evtchn *chn)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_evtchn_status()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[49]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_evtchn_status()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_evtchn_status()\n");
        return 1;
    }

    return ret;
}

static int cap_evtchn_reset (struct domain *d1, struct domain *d2)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d1->cap_flag == 0 || d2->cap_flag == 0 )		
        return 0;

	TT_DBG_ON(cap_debug,"In cap_evtchn_reset()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[50]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_evtchn_reset()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_evtchn_reset()\n");
        return 1;
    }

    return ret;

}

static int cap_alloc_security_evtchn (struct evtchn *chn)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_alloc_security_evtchn()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[51]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_alloc_security_evtchn)\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_alloc_security_evtchn()\n");
        return 1;
    }

    return ret;
}


static void cap_free_security_evtchn (struct evtchn *chn)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return;
    if( current->domain->domain_id > 100 )
        return;
    if ( current->domain->cap_flag == 0 )
        return;

    TT_DBG_ON(cap_debug,"In cap_free_security_evtchn()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[52]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\n");
        return;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_free_security_evtchn()\n");
        return;
    }

    return;
}

static char *cap_show_security_evtchn (struct domain *d, const struct evtchn *chn)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return NULL;
    if( current->domain->domain_id > 100 )
        return NULL;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return NULL;

	TT_DBG_ON(cap_debug,"In cap_show_security_evtchn()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[53]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_show_security_evtchn()\n");
        return NULL;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_show_security_evtchn()\n");
        return NULL;
    }

    return NULL;
}

static int cap_get_pod_target(struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_get_pod_target()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[54]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_get_pod_target()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_get_pod_target()\n");
        return 1;
    }

    return ret;
}

static int cap_set_pod_target(struct domain *d)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_set_pod_target()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[55]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_set_pod_target()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_set_pod_target()\n");
        return 1;
    }

    return ret;
}

static int cap_resource_plug_core (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_resource_plug_code()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[56]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_resource_plug_core()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_resource_plug_core()\n");
        return 1;
    }

    return ret;
}

static int cap_resource_unplug_core (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_resource_unplug_core()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[57]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_resource_unplug_core()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_resource_unplug_core()\n");
        return 1;
    }

    return ret;
}

static int cap_resource_plug_pci (uint32_t machine_bdf)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_resource_plug_pci()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[58]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_resource_plug_pci()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_resource_plug_pci()\n");
        return 1;
    }

    return ret;
}

static int cap_resource_unplug_pci (uint32_t machine_bdf)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_resource_unplug_core()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[59]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_resource_unplug_pci()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_resource_unplug_pci()\n");
        return 1;
    }

    return ret;
}

static int cap_resource_setup_pci (uint32_t machine_bdf)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_resource_setup_pci()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[60]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_resource_setup_pci()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_resource_setup_pci()\n");
        return 1;
    }

    return ret;
}

static int cap_resource_setup_gsi (int gsi)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_resource_setup_gsi()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[61]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_resource_setup_gsi()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_resource_setup_gsi()\n");
        return 1;
    }

    return ret;
}

static int cap_resource_setup_misc (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_resource_setup_misc()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[62]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_resource_setup_misc()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_resource_setup_misc()\n");
        return 1;
    }

    return ret;
}

static int cap_page_offline (uint32_t cmd)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_page_offline()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[63]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_page_offline()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_page_offline()\n");
        return 1;
    }

    return ret;
}

static int cap_lockprof (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_lockprof()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[64]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_lockprof()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_lockprof()\n");
        return 1;
    }

    return ret;
}

static int cap_cpupool_op (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_cpupool_op()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[65]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_cpupool_op()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_cpupool_op()\n");
        return 1;
    }

    return ret;
}

static int cap_sched_op (void)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 )
        return 0;

    TT_DBG_ON(cap_debug,"In cap_sched_op()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[66]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_sched_op()\n");
        return 0;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_sched_op()\n");
        return 1;
    }

    return ret;
}

#if 0
static long cap___do_xsm_op(XEN_GUEST_HANDLE(xsm_op_t) op)
{
    return -ENOSYS;
}
#endif

static char *cap_show_irq_sid (int irq)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return NULL;
    if( current->domain->domain_id > 100 )
        return NULL;
    if ( current->domain->cap_flag == 0 )
        return NULL;

    TT_DBG_ON(cap_debug,"In cap_show_irq_sid()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[67]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
        TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_show_irq_sid()\n");
        return NULL;
    }
    else
    {
        TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_show_irq_sid()\n");
        return NULL;
    }

    return NULL;
}

static int cap_map_domain_pirq (struct domain *d, int irq, void *data)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_map_domain_pirq()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[68]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_map_domain_pirq()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_map_domain_pirq()\n");
        return 1;
    }

    return ret;
	
}

static int cap_irq_permission (struct domain *d, int pirq, uint8_t allow)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_irq_permission()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[69]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_irq_permission()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_irq_permission()\n");
        return 1;
    }

    return ret;
}


static int cap_iomem_permission (struct domain *d, uint64_t s, uint64_t e, uint8_t allow)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_iomem_permission()\n");

    cap = &(CAP_BOOT_INFO->cap_hypercalls[70]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_iomem_permission()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_iomem_permission()\n");
        return 1;
    }

    return ret;

}

static int cap_pci_config_permission (struct domain *d, uint32_t machine_bdf,
                                        uint16_t start, uint16_t end,
                                        uint8_t access)
{
    int ret = 0;

    struct capability *cap;

    if ( IS_PRIV(current->domain) )
        return 0;
    if( current->domain->domain_id > 100 )
        return 0;
    if ( current->domain->cap_flag == 0 || d->cap_flag == 0 )
        return 0;

	TT_DBG_ON(cap_debug,"In cap_pci_config_permission()\n");
    cap = &(CAP_BOOT_INFO->cap_hypercalls[71]);

    if ( (ret =  cap_check(current->domain, cap)) == 1 )
    {
		TT_DBG_ON(cap_debug,"cap_check success\nLeaving cap_pci_config_permission()\n");
        return 0;
    }
    else
    {
		TT_DBG_ON(cap_debug,"cap_check failed!\nLeaving cap_pci_config_permission()\n");
        return 1;
    }

    return ret;

}

#if 0
#ifdef CONFIG_X86
static int cap_shadow_control (struct domain *d, uint32_t op)
{
    return 0;
}

static int cap_getpageframeinfo (struct domain *d)
{
    return 0;
}

static int cap_getmemlist (struct domain *d)
{
    return 0;
}

static int cap_hypercall_init (struct domain *d)
{
    return 0;
}

static int cap_hvmcontext (struct domain *d, uint32_t cmd)
{
    return 0;
}

static int cap_address_size (struct domain *d, uint32_t cmd)
{
    return 0;
}

static int cap_machine_address_size (struct domain *d, uint32_t cmd)
{
    return 0;
}

static int cap_hvm_param (struct domain *d, unsigned long op)
{
    return 0;
}

static int cap_hvm_set_pci_intx_level (struct domain *d)
{
    return 0;
}

static int cap_hvm_set_isa_irq_level (struct domain *d)
{
    return 0;
}

static int cap_hvm_set_pci_link_route (struct domain *d)
{
    return 0;
}

static int cap_hvm_inject_msi (struct domain *d)
{
    return 0;
}

static int cap_mem_event (struct domain *d)
{
    return 0;
}

static int cap_mem_sharing (struct domain *d)
{
    return 0;
}

static int cap_apic (struct domain *d, int cmd)
{
    return 0;
}

static int cap_xen_settime (void)
{
    return 0;
}

static int cap_memtype (uint32_t access)
{
    return 0;
}

static int cap_microcode (void)
{
    return 0;
}

static int cap_physinfo (void)
{
    return 0;
}

static int cap_platform_quirk (uint32_t quirk)
{
    return 0;
}

static int cap_firmware_info (void)
{
    return 0;
}

static int cap_efi_call(void)
{
    return 0;
}

static int cap_acpi_sleep (void)
{
    return 0;
}

static int cap_change_freq (void)
{
    return 0;
}

static int cap_getidletime (void)
{
    return 0;
}

static int cap_machine_memory_map (void)
{
    return 0;
}

static int cap_mmu_normal_update (struct domain *d, struct domain *t,
                                    struct domain *f, intpte_t fpte)
{
    return 0;
}

static int cap_mmu_machphys_update (struct domain *d, struct domain *f, unsigned long mfn)
{
    return 0;
}

static int cap_update_va_mapping (struct domain *d, struct domain *f, 
                                                            l1_pgentry_t pte)
{
    return 0;
}

static int cap_add_to_physmap (struct domain *d1, struct domain *d2)
{
    return 0;
}

static int cap_remove_from_physmap (struct domain *d1, struct domain *d2)
{
    return 0;
}

static int cap_sendtrigger (struct domain *d)
{
    return 0;
}

static int cap_bind_pt_irq (struct domain *d, struct xen_domctl_bind_pt_irq *bind)
{
    return 0;
}

static int cap_unbind_pt_irq (struct domain *d)
{
    return 0;
}

static int cap_pin_mem_cacheattr (struct domain *d)
{
    return 0;
}

static int cap_ext_vcpucontext (struct domain *d, uint32_t cmd)
{
    return 0;
}

static int cap_vcpuextstate (struct domain *d, uint32_t cmd)
{
    return 0;
}

static int cap_ioport_permission (struct domain *d, uint32_t s, uint32_t e, uint8_t allow)
{
    return 0;
}


#endif
#endif

//long do_cap_op(XEN_GUEST_HANDLE(xsm_op_t) u_cap_op);

static struct xsm_operations cap_ops = {
    .security_domaininfo = cap_security_domaininfo,
    .setvcpucontext = cap_setvcpucontext,
    .pausedomain = cap_pausedomain,
    .unpausedomain = cap_unpausedomain,    
    .resumedomain = cap_resumedomain,    
    .domain_create = cap_domain_create,
    .max_vcpus = cap_max_vcpus,
    .destroydomain = cap_destroydomain,
    .vcpuaffinity = cap_vcpuaffinity,
    .scheduler = cap_scheduler,
    .getdomaininfo = cap_getdomaininfo,
    .getvcpucontext = cap_getvcpucontext,
    .getvcpuinfo = cap_getvcpuinfo,
    .domain_settime = cap_domain_settime,
    .set_target = cap_set_target,
    .domctl = cap_domctl,
    .set_virq_handler = cap_set_virq_handler,
    .tbufcontrol = cap_tbufcontrol,
    .readconsole = cap_readconsole,
    .sched_id = cap_sched_id,
    .setdomainmaxmem = cap_setdomainmaxmem,
    .setdomainhandle = cap_setdomainhandle,
    .setdebugging = cap_setdebugging,
    .perfcontrol = cap_perfcontrol,
    .debug_keys = cap_debug_keys,
    .getcpuinfo = cap_getcpuinfo,
    .availheap = cap_availheap,
    .get_pmstat = cap_get_pmstat,
    .setpminfo = cap_setpminfo,
    .pm_op = cap_pm_op,
    .do_mca = cap_do_mca,

    .evtchn_unbound = cap_evtchn_unbound,
    .evtchn_interdomain = cap_evtchn_interdomain,
    .evtchn_close_post = cap_evtchn_close_post,
    .evtchn_send = cap_evtchn_send,
    .evtchn_status = cap_evtchn_status,
    .evtchn_reset = cap_evtchn_reset,

    .grant_mapref = cap_grant_mapref,
    .grant_unmapref = cap_grant_unmapref,
    .grant_setup = cap_grant_setup,
    .grant_transfer = cap_grant_transfer,
    .grant_copy = cap_grant_copy,
    .grant_query_size = cap_grant_query_size,

    //.alloc_security_domain = cap_domain_alloc_security,
    //.free_security_domain = cap_domain_free_security,
    .alloc_security_evtchn = cap_alloc_security_evtchn,
    .free_security_evtchn = cap_free_security_evtchn,
    .show_security_evtchn = cap_show_security_evtchn,

    .get_pod_target = cap_get_pod_target,
    .set_pod_target = cap_set_pod_target,
    .memory_adjust_reservation = cap_memory_adjust_reservation,
    .memory_stat_reservation = cap_memory_stat_reservation,
    .memory_pin_page = cap_memory_pin_page,

    .console_io = cap_console_io,

    .profile = cap_profile,

    .kexec = cap_kexec,
    .schedop_shutdown = cap_schedop_shutdown,

    .show_irq_sid = cap_show_irq_sid,

    .map_domain_pirq = cap_map_domain_pirq,
    .irq_permission = cap_irq_permission,
    .iomem_permission = cap_iomem_permission,
    .pci_config_permission = cap_pci_config_permission,

    .resource_plug_core = cap_resource_plug_core,
    .resource_unplug_core = cap_resource_unplug_core,
    .resource_plug_pci = cap_resource_plug_pci,
    .resource_unplug_pci = cap_resource_unplug_pci,
    .resource_setup_pci = cap_resource_setup_pci,
    .resource_setup_gsi = cap_resource_setup_gsi,
    .resource_setup_misc = cap_resource_setup_misc,

    .page_offline = cap_page_offline,
    .lockprof = cap_lockprof,
    .cpupool_op = cap_cpupool_op,
    .sched_op = cap_sched_op,
    //.__do_xsm_op = do_cap_op,
#if 0
#ifdef CONFIG_X86
    .shadow_control = cap_shadow_control,
    .getpageframeinfo = cap_getpageframeinfo,
    .getmemlist = cap_getmemlist,
    .hypercall_init = cap_hypercall_init,
    .hvmcontext = cap_hvmcontext,
    .address_size = cap_address_size,
    .hvm_param = cap_hvm_param,
    .hvm_set_pci_intx_level = cap_hvm_set_pci_intx_level,
    .hvm_set_isa_irq_level = cap_hvm_set_isa_irq_level,
    .hvm_set_pci_link_route = cap_hvm_set_pci_link_route,
    .mem_event = cap_mem_event,
    .mem_sharing = cap_mem_sharing,
    .apic = cap_apic,
    .xen_settime = cap_xen_settime,
    .memtype = cap_memtype,
    .microcode = cap_microcode,
    .physinfo = cap_physinfo,
    .platform_quirk = cap_platform_quirk,
    .firmware_info = cap_firmware_info,
    .efi_call = cap_efi_call,
    .acpi_sleep = cap_acpi_sleep,
    .change_freq = cap_change_freq,
    .getidletime = cap_getidletime,
    .machine_memory_map = cap_machine_memory_map,
    .domain_memory_map = cap_domain_memory_map,
    .mmu_normal_update = cap_mmu_normal_update,
    .mmu_machphys_update = cap_mmu_machphys_update,
    .update_va_mapping = cap_update_va_mapping,
    .add_to_physmap = cap_add_to_physmap,
    .remove_from_physmap = cap_remove_from_physmap,
    .sendtrigger = cap_sendtrigger,
    .get_device_group = cap_get_device_group,
    .test_assign_device = cap_test_assign_device,
    .assign_device = cap_assign_device,
    .deassign_device = cap_deassign_device,
    .bind_pt_irq = cap_bind_pt_irq,
    .unbind_pt_irq = cap_unbind_pt_irq,
    .pin_mem_cacheattr = cap_pin_mem_cacheattr,
    .ext_vcpucontext = cap_ext_vcpucontext,
    .vcpuextstate = cap_vcpuextstate,
    .ioport_permission = cap_ioport_permission,
#endif
#endif	
};

static __init int xsm_cap_init(void)
{
	int ret = 0;

	TT_DBG_ON(cap_debug,"Xen-cap: Initializing and registering with XSM.\n");

	original_ops = xsm_ops;

	if ( register_xsm(&cap_ops) )
		TT_DBG_ON(cap_debug,"xen-cap: Unable to register with XSM.\n");

	TT_DBG_ON(cap_debug,"Leaving xsm_cap_init.\n");
	return ret;
}

xsm_initcall(xsm_cap_init);
