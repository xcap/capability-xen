/**
 * This file is a part of the Flux Xen Capabilities infrastructure.
 *
 * Capabilities Support for Xen
 *
 * Author: Yathindra Naik
 * Date:   September 2012
 */

#ifndef __XEN_XEN_CAP_H__
#define __XEN_XEN_CAP_H__

#include <xen/sched.h>
#include <public/xen-cap.h>
//#include <xen/lib.h>

/* Capability tokens are random numbers for now. This implementation is taken from wikipedia.
 * Source: http://en.wikipedia.org/wiki/Multiply-with-carry
 * This is based on Multiply-with-carry random number generation algorithm invented by George Marsaglia.
 */


extern int cap_debug;
extern struct domain *d;

/* This is a simple structure for capabilities. */
/* It is on a list for now. Later I'll consider a more efficient structure. */
//struct capability 
//{
//	uint32_t magic; 
	/* We may need to encode the rights here too ? */
	/* CAP_READ, CAP_WRITE, CAP_SHARE etc */
	//struct list_head cap_list;
//};

//struct cap_boot_info
//{
//	struct capability cap_hypercalls[NUM_HYPERCALLS];

//};

extern struct cap_boot_info *CAP_BOOT_INFO;

/* Pointer to capability boot info page */
//struct cap_boot_info *CAP_BOOT_INFO;

/* Domains maintain their capabilities in cap_space */
//struct cap_space
//{
	/* Different resources maintain their capabilities
	 * in efficient lookup structures. Hence, we need
	 * to have different pointers.
	 */
//	int num_caps;
//	struct capability *caps;
//};

#define CAP_INIT(x,y) { (x), (y) }

#define CAP_DEFINE(name,x,y) \
	struct capability cap_##name = CAP_INIT(x,y)
									
int cap_init(void);
int cap_init_hypercalls(void);
int cap_share_boot_info_page(void);
int cap_create(struct capability *cap);
int cap_grant(struct domain *d, struct capability *list, int size );
int cap_check(struct domain *d, struct capability *cap);
int dom0_cap_create(struct domain *mydom, struct capability *cap);
void get_caps_hypercall(char *tok, struct capability *cap);
extern void init_rand(uint32_t x);
extern uint32_t rand_cmwc(void);

#endif /* __XEN_XEN_CAP_H__ */



