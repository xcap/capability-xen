#ifndef __LIB_H__
#define __LIB_H__

#include <xen/inttypes.h>
#include <xen/stdarg.h>
#include <xen/config.h>
#include <xen/types.h>
#include <xen/xmalloc.h>
#include <xen/string.h>
#include <asm/bug.h>

void __bug(char *file, int line) __attribute__((noreturn));
void __warn(char *file, int line);

#define BUG_ON(p)  do { if (unlikely(p)) BUG();  } while (0)
#define WARN_ON(p) do { if (unlikely(p)) WARN(); } while (0)

#if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)
/* Force a compilation error if condition is true */
#define BUILD_BUG_ON(cond) ({ _Static_assert(!(cond), "!(" #cond ")"); })

/* Force a compilation error if condition is true, but also produce a
   result (of value 0 and type size_t), so the expression can be used
   e.g. in a structure initializer (or where-ever else comma expressions
   aren't permitted). */
#define BUILD_BUG_ON_ZERO(cond) \
    sizeof(struct { _Static_assert(!(cond), "!(" #cond ")"); })
#else
#define BUILD_BUG_ON_ZERO(cond) sizeof(struct { int:-!!(cond); })
#define BUILD_BUG_ON(cond) ((void)BUILD_BUG_ON_ZERO(cond))
#endif

#ifndef assert_failed
#define assert_failed(p)                                        \
do {                                                            \
    printk("Assertion '%s' failed, line %d, file %s\n", p ,     \
                   __LINE__, __FILE__);                         \
    BUG();                                                      \
} while (0)
#endif

#ifndef NDEBUG
#define ASSERT(p) \
    do { if ( unlikely(!(p)) ) assert_failed(#p); } while (0)
#else
#define ASSERT(p) do { if ( 0 && (p) ); } while (0)
#endif

#define ABS(_x) ({                              \
    typeof(_x) __x = (_x);                      \
    (__x < 0) ? -__x : __x;                     \
})

#define SWAP(_a, _b) \
   do { typeof(_a) _t = (_a); (_a) = (_b); (_b) = _t; } while ( 0 )

#define DIV_ROUND(n, d) (((n) + (d) / 2) / (d))
#define DIV_ROUND_UP(n, d) (((n) + (d) - 1) / (d))

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]) + __must_be_array(x))

#define reserve_bootmem(_p,_l) ((void)0)

struct domain;

void cmdline_parse(const char *cmdline);
int parse_bool(const char *s);

/*#define DEBUG_TRACE_DUMP*/
#ifdef DEBUG_TRACE_DUMP
extern void debugtrace_dump(void);
extern void debugtrace_printk(const char *fmt, ...);
#else
#define debugtrace_dump()          ((void)0)
#define debugtrace_printk(_f, ...) ((void)0)
#endif

/* Allows us to use '%p' as general-purpose machine-word format char. */
#define _p(_x) ((void *)(unsigned long)(_x))
extern void printk(const char *format, ...)
    __attribute__ ((format (printf, 1, 2)));
extern void panic(const char *format, ...)
    __attribute__ ((format (printf, 1, 2)));
extern long vm_assist(struct domain *, unsigned int, unsigned int);
extern int __printk_ratelimit(int ratelimit_ms, int ratelimit_burst);
extern int printk_ratelimit(void);

#ifdef CONFIG_XENCAP

    #define TT_DBG(_f, _a...)   do {                                                \
        printk("TT_LOG:(cpu(%d): %s) "                                              \
                _f, smp_processor_id(), __FUNCTION__, ## _a);                       \
    } while (0)

    #define TT_DBG_ON(_g, _f, _a...) do {                                           \
            if (_g) {                                                               \
                TT_DBG(_f, ## _a);                                                  \
            }                                                                       \
    } while (0)

    #define TT_WARN(_f, _a...)  do {                                                \
        /* console_force_unlock();  */                                              \
        printk("TT_LOG:(cpu(%d): %s) " TXT_FG_YELLOW "Warrning:" TXT_FG_WHITE       \
                _f, smp_processor_id(), __FUNCTION__, ## _a);                       \
    } while (0)

    #define TT_WARN_ON(_g, _f, _a...) do {                                          \
            if (_g) {                                                               \
                TT_WARN(_f, ## _a);                                                 \
            }                                                                       \
    } while (0)

    #define TT_WARN_ON_ONCE(_g, _f, _a...)                                          \
            ({                                                                      \
            static int __warn_once = 1;                                             \
            int __ret = 0;                                                          \
                                                                                    \
            if (unlikely((_g) && __warn_once)) {                                    \
                __warn_once = 0;                                                    \
                TT_WARN(_f, ## _a);                                                 \
                __ret = 1;                                                          \
            }                                                                       \
            __ret;                                                                  \
    })


    #define TTD_DO_ON(__condition, __action)                                        \
    ({                                                                              \
            int __ret = 0;                                                          \
                                                                                    \
            if (unlikely(__condition)) {                                            \
                (__action);                                                         \
                __ret = 1;                                                          \
            }                                                                       \
            __ret;                                                                  \
     })
    

    #define TTD_DO_ONCE(__action)                                                   \
    ({                                                                              \
            static int __warn_once = 1;                                             \
            int __ret = 0;                                                          \
                                                                                    \
            if (unlikely(__warn_once)) {                                            \
                (__action);                                                         \
                __warn_once = 0;                                                    \
                __ret = 1;                                                          \
            }                                                                       \
            __ret;                                                                  \
     })

    #define TTD_DO_ONCE_ON(__condition, __action)                                   \
    ({                                                                              \
            static int __warn_once = 1;                                             \
            int __ret = 0;                                                          \
                                                                                    \
            if (unlikely(__warn_once && __condition)) {                             \
                (__action);                                                         \
                __warn_once = 0;                                                    \
                __ret = 1;                                                          \
            }                                                                       \
            __ret;                                                                  \
     })



    #define TT_ERR(_f, _a...)   do {                                                \
        /* console_force_unlock();  */                                              \
        printk("TT_LOG:(cpu(%d): %s) " TXT_FG_RED "Error:" TXT_FG_WHITE             \
                _f, smp_processor_id(), __FUNCTION__, ## _a);                       \
    } while (0)

    #define TT_ERR_ON(_g, _f, _a...) do {                                           \
            if (_g) {                                                               \
                TT_ERR(_f, ## _a);                                                  \
            }                                                                       \
    } while (0)

    #define TT_DBG_DUMP( _p, _len_p, _max_len) do {                                 \
        int _i, _j;                                                                 \
        unsigned long _len = _len_p;                                                \
                                                                                    \
        if ( (_max_len) && (_len > _max_len)) {                                     \
            _len = _max_len;                                                        \
            printk("Buffer exceeds max length, dumping first %i bytes\n", _max_len);\
        }                                                                           \
                                                                                    \
        for ( _i = 0; _i < (_len); ) {                                              \
            for ( _j = 0; ( _j < 16) && (_i < (_len)); _j++, _i++ ) {               \
                printk("%02x ", (unsigned char)*((char *)(_p) + _i) );              \
            }                                                                       \
            printk("\n");                                                           \
        }                                                                           \
        printk("\n");                                                               \
    } while (0)

    #define TT_DBG_DUMP_ON(_g, _p, _len_p, _max_len) do {                           \
            if (_g) {                                                               \
                TT_DBG_DUMP(_p, _len_p, _max_len);                                  \
            }                                                                       \
    } while (0)

    #define TT_BUG(_f, _a...)   do {                                                \
        printk("TT_BUG:(cpu(%d): %s) "                                              \
                _f, smp_processor_id(), __FUNCTION__, ## _a);                       \
        dump_execution_state();                                                     \
    } while (0)

          
    /*
     * Escape codes for controlling text color, brightness, etc.
     */

    #define TXT_CLRSCREEN           "\e[2J"
    #define TXT_NORMAL              "\e[0m"
    #define TXT_BRIGHT              "\e[1m"
    #define TXT_REVERSED            "\e[7m"
    #define TXT_FG_BLACK            "\e[30m"
    #define TXT_FG_RED              "\e[31m"
    #define TXT_FG_GREEN            "\e[32m"
    #define TXT_FG_YELLOW           "\e[33m"
    #define TXT_FG_BLUE             "\e[34m"
    #define TXT_FG_MAGENTA          "\e[35m"
    #define TXT_FG_CYAN             "\e[36m"
    #define TXT_FG_WHITE            "\e[37m"
    #define TXT_BG_BLACK            "\e[40m"
    #define TXT_BG_RED              "\e[41m"
    #define TXT_BG_GREEN            "\e[42m"
    #define TXT_BG_YELLOW           "\e[43m"
    #define TXT_BG_BLUE             "\e[44m"
    #define TXT_BG_MAGENTA          "\e[45m"
    #define TXT_BG_CYAN             "\e[46m"
    #define TXT_BG_WHITE            "\e[47m"

#else
    #define TT_DBG(_f, _a...)       do { } while (0)
    #define TT_DBG_ON(_g, _f, _a...)    do { } while (0)
#endif /* CONFIG_XENCAP */

/* vsprintf.c */
#define sprintf __xen_has_no_sprintf__
#define vsprintf __xen_has_no_vsprintf__
extern int snprintf(char * buf, size_t size, const char * fmt, ...)
    __attribute__ ((format (printf, 3, 4)));
extern int vsnprintf(char *buf, size_t size, const char *fmt, va_list args)
    __attribute__ ((format (printf, 3, 0)));
extern int scnprintf(char * buf, size_t size, const char * fmt, ...)
    __attribute__ ((format (printf, 3, 4)));
extern int vscnprintf(char *buf, size_t size, const char *fmt, va_list args)
    __attribute__ ((format (printf, 3, 0)));

long simple_strtol(
    const char *cp,const char **endp, unsigned int base);
unsigned long simple_strtoul(
    const char *cp,const char **endp, unsigned int base);
long long simple_strtoll(
    const char *cp,const char **endp, unsigned int base);
unsigned long long simple_strtoull(
    const char *cp,const char **endp, unsigned int base);

unsigned long long parse_size_and_unit(const char *s, const char **ps);

uint64_t muldiv64(uint64_t a, uint32_t b, uint32_t c);

#define TAINT_UNSAFE_SMP                (1<<0)
#define TAINT_MACHINE_CHECK             (1<<1)
#define TAINT_BAD_PAGE                  (1<<2)
#define TAINT_SYNC_CONSOLE              (1<<3)
#define TAINT_ERROR_INJECT              (1<<4)
extern int tainted;
#define TAINT_STRING_MAX_LEN            20
extern char *print_tainted(char *str);
extern void add_taint(unsigned);

struct cpu_user_regs;
void dump_execstate(struct cpu_user_regs *);

#endif /* __LIB_H__ */

