/**
 * This file is a part of the Flux Xen Capabilities infrastructure.
 *
 * Capabilities Support for Xen
 *
 * Author: Yathindra Naik
 * Date:   September 2012
 */
#ifndef __CAP_H__
#define __CAP_H__

/* This is a simple structure for capabilities. */
/* It is on a list for now. Later I'll consider a more efficient structure. */
struct capability 
{
	uint32_t magic; 
	/* We may need to encode the rights here too ? */
	/* CAP_READ, CAP_WRITE, CAP_SHARE etc */
};

#endif
