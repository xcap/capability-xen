/**
 * This file is a part of the Flux Xen Capabilities infrastructure.
 *
 * Capabilities Support for Xen
 *
 * Author: Yathindra Naik
 * Date:   September 2012
 */

#ifndef __XEN_PUBLIC_XEN_CAP_H__
#define __XEN_PUBLIC_XEN_CAP_H__

#include "xen.h"

#define PHI 0x9e3779b9
#define NUM_HYPERCALLS 100 
#define NUM_CAPS 	100000
#define SEED 41 /* initial seed valure for RNG */

/* Capability tokens are random numbers for now. This implementation is taken from wikipedia.
 * Source: http://en.wikipedia.org/wiki/Multiply-with-carry
 * This is based on Multiply-with-carry random number generation algorithm invented by George Marsaglia.
 */

/* This is a simple structure for capabilities. */
/* It is on a list for now. Later I'll consider a more efficient structure. */
struct capability 
{
	uint32_t magic; 
	/* We may need to encode the rights here too ? */
	/* CAP_READ, CAP_WRITE, CAP_SHARE etc */
};

struct cap_boot_info
{
      struct capability cap_hypercalls[NUM_HYPERCALLS];

};

typedef struct capability xen_capability_t;
DEFINE_XEN_GUEST_HANDLE(xen_capability_t);

/* Domains maintain their capabilities in cap_space */
struct cap_space
{
	/* Different resources maintain their capabilities
	 * in efficient lookup structures. Hence, we need
	 * to have different pointers.
	 */
	int num_caps;
	struct capability *caps;
};

#endif /* __XEN_PUBLIC_XEN_CAP_H__ */
