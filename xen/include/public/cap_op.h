/*
 * cap_op.h
 * 
 * This declares Capability related hypercalls.
 *
 */

#ifndef __XEN_CAP_OP_H__
#define __XEN_CAP_OP_H__

#include "xen.h"
#include "xen-cap.h"

#define XEN_CAP_OP_INTERFACE_VERSION 1

struct xen_cap_op_cap_create {
	struct capability *cap;
};
typedef struct xen_cap_op_cap_create xen_cap_op_cap_create_t;
DEFINE_XEN_GUEST_HANDLE(xen_cap_op_cap_create_t);

struct xen_cap_op_cap_grant {
	int to;
/*	XEN_GUEST_HANDLE(xen_capability_t) list; */
	struct capability *list;
	int size;
};
typedef struct xen_cap_op_cap_grant xen_cap_op_cap_grant_t;
DEFINE_XEN_GUEST_HANDLE(xen_cap_op_cap_grant_t);

struct xen_cap_op_cap_check {
	struct capability *cap;
};
typedef struct xen_cap_op_cap_check xen_cap_op_cap_check_t;
DEFINE_XEN_GUEST_HANDLE(xen_cap_op_cap_check_t);

struct xen_cap_op_get_caps_hypercall {
	char *tok;
/*	XEN_GUEST_HANDLE(xen_capability_t) cap; */
	struct capability *cap;
};
typedef struct xen_cap_op_get_caps_hypercall xen_cap_op_get_caps_hypercall_t;
DEFINE_XEN_GUEST_HANDLE(xen_cap_op_get_caps_hypercall_t);

struct xen_cap_op {
    uint32_t cmd;
#define XEN_CAP_OP_cap_create				1	
#define XEN_CAP_OP_cap_grant				2
#define XEN_CAP_OP_cap_check				3
#define XEN_CAP_OP_get_caps_hypercall		4
    uint32_t interface_version; /* XEN_CAP_OP_INTERFACE_VERSION */
	domid_t  domain;
    union {
		struct xen_cap_op_cap_create			cap_create;
		struct xen_cap_op_cap_grant				cap_grant;
		struct xen_cap_op_cap_check				cap_check;
		struct xen_cap_op_get_caps_hypercall	get_caps_hypercall;
    } u;
};
typedef struct xen_cap_op xen_cap_op_t;
DEFINE_XEN_GUEST_HANDLE(xen_cap_op_t);

#endif /* __XEN_CAP_OP_H__ */


