/*
 * This is a test hypercall interface for Xen-Cap.
 */
#include <err.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <xenctrl.h>
#include <xenguest.h>
#include <xc_private.h>

int main(int argc, char **argv)
{
	xc_interface *xch;
	int ret, input_number;

	if ( (argc != 2) )
	errx(1, "usage: %s input_number ", argv[0]);

	xch = xc_interface_open(0,0,0);
	if ( !xch ){
		errx(1, "xcutils: xc_xen_cap.c: failed to open control interface");
	}

	input_number= atoi(argv[1]);
	printf("input_number: %d\n", input_number);

	ret = do_cap_op(xch, input_number);
	printf("return value: %d\n", ret);

	if ( ret == 0 )
	{
		errx(1, "ret == 0\n");
	    fflush(stdout);
    }

    xc_interface_close(xch);

    return ret;
}

 
