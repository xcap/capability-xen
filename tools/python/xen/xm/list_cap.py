
"""Request for capabilities.
"""

from xen.xm.opts import OptionError
from xen.xm import main as xm_main
from xen.xm.main import server
from xen.util import xsconstants

def help():
    return """
    Usage: xm list_cap

    List the capabilities in this domains cspace.
    Xen-Cap module."""

def list_cap():
	return ""

def main(argv): 
    if "-?" in argv:
        help()
        return

    list_cap()

if __name__ == '__main__':
    try:
        main(sys.argv)
    except Exception, e:
        sys.stderr.write('Error: %s\n' % str(e))    
        sys.exit(-1)

    
