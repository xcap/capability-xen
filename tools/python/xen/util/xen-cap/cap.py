import sys
from xen.util import xsconstants
from xen.xend.XendLogging import log

class XSMError(Exception):
    def __init__(self,value):
        self.value = value
    def __str__(self):
        return repr(self.value)


#Functions exported through XML-RPC
xmlrpc_exports = [
  'set_cap',
  'get_cap',
  'list_cap',
]

def err(msg):
    """Raise XSM-dummy exception.
    """
    raise XSMError(msg)

def set_cap():
	return ""

def get_cap():
	return ""

def list_cap():
	return ""
