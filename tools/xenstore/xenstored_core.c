/* 
    Simple prototype Xen Store Daemon providing simple tree-like database.
    Copyright (C) 2005 Rusty Russell IBM Corporation

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#ifndef NO_SOCKETS
#include <sys/socket.h>
#include <sys/un.h>
#endif
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <syslog.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <getopt.h>
#include <signal.h>
#include <assert.h>
#include <setjmp.h>

#include "utils.h"
#include "list.h"
#include "talloc.h"
#include "xenstore_lib.h"
#include "xenstored_core.h"
#include "xenstored_watch.h"
#include "xenstored_transaction.h"
#include "xenstored_domain.h"
#include "xenctrl.h"
#include "tdb.h"

#include "hashtable.h"

extern xc_evtchn *xce_handle; /* in xenstored_domain.c */

static bool verbose = false;
LIST_HEAD(connections);
static int tracefd = -1;
static bool recovery = true;
static bool remove_local = true;
static int reopen_log_pipe[2];
static char *tracefile = NULL;
static TDB_CONTEXT *tdb_ctx = NULL;

static void corrupt(struct connection *conn, const char *fmt, ...);
static void check_store(void);

#define log(...)							\
	do {								\
		char *s = talloc_asprintf(NULL, __VA_ARGS__);		\
		trace("%s\n", s);					\
		syslog(LOG_ERR, "%s",  s);				\
		talloc_free(s);						\
	} while (0)


int quota_nb_entry_per_domain = 1000;
int quota_nb_watch_per_domain = 128;
int quota_max_entry_size = 2048; /* 2K */
int quota_max_transaction = 10;

TDB_CONTEXT *tdb_context(struct connection *conn)
{
	/* conn = NULL used in manual_node at setup. */
	if (!conn || !conn->transaction)
		return tdb_ctx;
	return tdb_transaction_context(conn->transaction);
}

bool replace_tdb(const char *newname, TDB_CONTEXT *newtdb)
{
	if (!(tdb_ctx->flags & TDB_INTERNAL))
		if (rename(newname, xs_daemon_tdb()) != 0)
			return false;
	tdb_close(tdb_ctx);
	tdb_ctx = talloc_steal(talloc_autofree_context(), newtdb);
	return true;
}

static char *sockmsg_string(enum xsd_sockmsg_type type)
{
	switch (type) {
	case XS_DEBUG: return "DEBUG";
	case XS_DIRECTORY: return "DIRECTORY";
	case XS_READ: return "READ";
	case XS_GET_PERMS: return "GET_PERMS";
	case XS_WATCH: return "WATCH";
	case XS_UNWATCH: return "UNWATCH";
	case XS_TRANSACTION_START: return "TRANSACTION_START";
	case XS_TRANSACTION_END: return "TRANSACTION_END";
	case XS_INTRODUCE: return "INTRODUCE";
	case XS_RELEASE: return "RELEASE";
	case XS_GET_DOMAIN_PATH: return "GET_DOMAIN_PATH";
	case XS_WRITE: return "WRITE";
	case XS_MKDIR: return "MKDIR";
	case XS_RM: return "RM";
	case XS_SET_PERMS: return "SET_PERMS";
#ifdef CONFIG_XENCAP
	case XS_GET_CAPS: return "GET_CAPS";
#endif
	case XS_WATCH_EVENT: return "WATCH_EVENT";
	case XS_ERROR: return "ERROR";
	case XS_IS_DOMAIN_INTRODUCED: return "XS_IS_DOMAIN_INTRODUCED";
	case XS_RESUME: return "RESUME";
	case XS_SET_TARGET: return "SET_TARGET";
	case XS_RESET_WATCHES: return "RESET_WATCHES";
	default:
		return "**UNKNOWN**";
	}
}

void trace(const char *fmt, ...)
{
	va_list arglist;
	char *str;
	char sbuf[1024];
	int ret, dummy;

	if (tracefd < 0)
		return;

	/* try to use a static buffer */
	va_start(arglist, fmt);
	ret = vsnprintf(sbuf, 1024, fmt, arglist);
	va_end(arglist);

	if (ret <= 1024) {
		dummy = write(tracefd, sbuf, ret);
		return;
	}

	/* fail back to dynamic allocation */
	va_start(arglist, fmt);
	str = talloc_vasprintf(NULL, fmt, arglist);
	va_end(arglist);
	dummy = write(tracefd, str, strlen(str));
	talloc_free(str);
}

static void trace_io(const struct connection *conn,
		     const struct buffered_data *data,
		     int out)
{
	unsigned int i;
	time_t now;
	struct tm *tm;

#ifdef HAVE_DTRACE
	dtrace_io(conn, data, out);
#endif

	if (tracefd < 0)
		return;

	now = time(NULL);
	tm = localtime(&now);

	trace("%s %p %04d%02d%02d %02d:%02d:%02d %s (",
	      out ? "OUT" : "IN", conn,
	      tm->tm_year + 1900, tm->tm_mon + 1,
	      tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec,
	      sockmsg_string(data->hdr.msg.type));
	
	for (i = 0; i < data->hdr.msg.len; i++)
		trace("%c", (data->buffer[i] != '\0') ? data->buffer[i] : ' ');
	trace(")\n");
}

void trace_create(const void *data, const char *type)
{
	trace("CREATE %s %p\n", type, data);
}

void trace_destroy(const void *data, const char *type)
{
	trace("DESTROY %s %p\n", type, data);
}

/**
 * Signal handler for SIGHUP, which requests that the trace log is reopened
 * (in the main loop).  A single byte is written to reopen_log_pipe, to awaken
 * the select() in the main loop.
 */
static void trigger_reopen_log(int signal __attribute__((unused)))
{
	char c = 'A';
	int dummy;
	dummy = write(reopen_log_pipe[1], &c, 1);
}


static void reopen_log(void)
{
	if (tracefile) {
		if (tracefd > 0)
			close(tracefd);

		tracefd = open(tracefile, O_WRONLY|O_CREAT|O_APPEND, 0600);

		if (tracefd < 0)
			perror("Could not open tracefile");
		else
			trace("\n***\n");
	}
}

static bool write_messages(struct connection *conn)
{
	int ret;
	struct buffered_data *out;

	out = list_top(&conn->out_list, struct buffered_data, list);
	if (out == NULL)
		return true;

	if (out->inhdr) {
		if (verbose)
			xprintf("Writing msg %s (%.*s) out to %p\n",
				sockmsg_string(out->hdr.msg.type),
				out->hdr.msg.len,
				out->buffer, conn);
		ret = conn->write(conn, out->hdr.raw + out->used,
				  sizeof(out->hdr) - out->used);
		if (ret < 0)
			return false;

		out->used += ret;
		if (out->used < sizeof(out->hdr))
			return true;

		out->inhdr = false;
		out->used = 0;

		/* Second write might block if non-zero. */
		if (out->hdr.msg.len && !conn->domain)
			return true;
	}

	ret = conn->write(conn, out->buffer + out->used,
			  out->hdr.msg.len - out->used);
	if (ret < 0)
		return false;

	out->used += ret;
	if (out->used != out->hdr.msg.len)
		return true;

	trace_io(conn, out, 1);

	list_del(&out->list);
	talloc_free(out);

	return true;
}

static int destroy_conn(void *_conn)
{
	struct connection *conn = _conn;

	/* Flush outgoing if possible, but don't block. */
	if (!conn->domain) {
		fd_set set;
		struct timeval none;

		FD_ZERO(&set);
		FD_SET(conn->fd, &set);
		none.tv_sec = none.tv_usec = 0;

		while (!list_empty(&conn->out_list)
		       && select(conn->fd+1, NULL, &set, NULL, &none) == 1)
			if (!write_messages(conn))
				break;
		close(conn->fd);
	}
        if (conn->target)
                talloc_unlink(conn, conn->target);
	list_del(&conn->list);
	trace_destroy(conn, "connection");
	return 0;
}


static void set_fd(int fd, fd_set *set, int *max)
{
	if (fd < 0)
		return;
	FD_SET(fd, set);
	if (fd > *max)
		*max = fd;
}


static int initialize_set(fd_set *inset, fd_set *outset, int sock, int ro_sock,
			  struct timeval **ptimeout)
{
	static struct timeval zero_timeout = { 0 };
	struct connection *conn;
	int max = -1;

	*ptimeout = NULL;

	FD_ZERO(inset);
	FD_ZERO(outset);

	if (sock != -1)
		set_fd(sock, inset, &max);
	if (ro_sock != -1)
		set_fd(ro_sock, inset, &max);
	if (reopen_log_pipe[0] != -1)
		set_fd(reopen_log_pipe[0], inset, &max);

	if (xce_handle != NULL)
		set_fd(xc_evtchn_fd(xce_handle), inset, &max);

	list_for_each_entry(conn, &connections, list) {
		if (conn->domain) {
			if (domain_can_read(conn) ||
			    (domain_can_write(conn) &&
			     !list_empty(&conn->out_list)))
				*ptimeout = &zero_timeout;
		} else {
			set_fd(conn->fd, inset, &max);
			if (!list_empty(&conn->out_list))
				FD_SET(conn->fd, outset);
		}
	}

	return max;
}

/* Is child a subnode of parent, or equal? */
bool is_child(const char *child, const char *parent)
{
	unsigned int len = strlen(parent);

	/* / should really be "" for this algorithm to work, but that's a
	 * usability nightmare. */
	if (streq(parent, "/"))
		return true;

	if (strncmp(child, parent, len) != 0)
		return false;

	return child[len] == '/' || child[len] == '\0';
}

/* If it fails, returns NULL and sets errno. */
static struct node *read_node(struct connection *conn, const char *name)
{
	TDB_DATA key, data;
	uint32_t *p;
	struct node *node;
	TDB_CONTEXT * context = tdb_context(conn);

	key.dptr = (void *)name;
	key.dsize = strlen(name);
	data = tdb_fetch(context, key);

	if (data.dptr == NULL) {
		if (tdb_error(context) == TDB_ERR_NOEXIST)
			errno = ENOENT;
		else {
			log("TDB error on read: %s", tdb_errorstr(context));
			errno = EIO;
		}
		return NULL;
	}

	node = talloc(name, struct node);
	node->name = talloc_strdup(node, name);
	node->parent = NULL;
	node->tdb = tdb_context(conn);
	talloc_steal(node, data.dptr);

	/* Datalen, childlen, number of permissions */
	p = (uint32_t *)data.dptr;
	node->num_perms = p[0];
	node->datalen = p[1];
	node->childlen = p[2];
#ifdef CONFIG_XENCAP
	node->num_caps = p[3];
#endif
	/* Permissions are struct xs_permissions. */
	node->perms = (void *)&p[4];

#ifdef CONFIG_XENCAP
	/* Capabilites are struct capability. */
	node->caps 	= (struct capability *)(node->perms + node->num_perms);
#endif
	/* Data is binary blob (usually ascii, no nul). */
	node->data = node->caps + node->num_caps;
	/* Children is strings, nul separated. */
	node->children = node->data + node->datalen;
#if 0
	log("==================== read_node ======================\n");
	log("num_perms: %d\n",node->num_perms);
	log("datalen: %d\n",node->datalen);
	log("childlen: %d\n",node->childlen);
	log("num_caps: %d\n",node->num_caps);
	log("skipping perms and caps\n");
	log("data: %s\n",(char*)node->data);
    for (i = 0; i < node->childlen; i += strlen(node->data+i)+1)
		        log("\t-> %s\n", ((char*)node->data+i));
#endif
	//log("read_node: %s returning node %p\n",name, node);
	return node;
}

#ifdef CONFIG_XENCAP
#if 0
static void debug_xencap(TDB_DATA key, TDB_DATA data, const struct node *node)
{
	void *p;
	int i, num_perms, num_caps, datalen, childlen;

	log("=============== Writing node ==============\n");
	log("key: %s\n",key.dptr);
	//log("Total data size: %d\n",(*(uint32_t *)data.dsize));
	log("num_perms: %d\n",(num_perms=((uint32_t *)data.dptr)[0]));
	log("data len: %d\n",(datalen=((uint32_t *)data.dptr)[1]));
	log("child len: %d\n",(childlen=((uint32_t *)data.dptr)[2]));
	log("num_caps: %d\n",(num_caps=((uint32_t *)data.dptr)[3]));

	p = data.dptr + 4 * sizeof(uint32_t);

	log("skipping permissions\n");
	p += num_perms*sizeof(node->perms[0]);
	
	log("skipping caps\n");
	p += num_caps*sizeof(node->caps[0]);

	if(datalen)
		log("Data: %s\n",((char*)p));
	
	p += datalen;

	for (i = 0; i < childlen; i += strlen(p+i)+1)
		log("\t-> %s\n", ((char*)p+i));
	
}
#endif
#endif

static bool write_node(struct connection *conn, const struct node *node)
{
	/*
	 * conn will be null when this is called from manual_node.
	 * tdb_context copes with this.
	 */

	TDB_DATA key, data;
	void *p;

	key.dptr = (void *)node->name;
	key.dsize = strlen(node->name);

	log("write_node: node.name %s\n",node->name);

	data.dsize = 6*sizeof(uint32_t) 
			+ node->num_perms*sizeof(node->perms[0])
			+ node->num_caps*sizeof(node->caps[0])
			+ node->datalen + node->childlen;

	data.dptr = talloc_size(node, data.dsize);
	((uint32_t *)data.dptr)[0] = node->num_perms;
	((uint32_t *)data.dptr)[1] = node->datalen;
	((uint32_t *)data.dptr)[2] = node->childlen;
#ifdef CONFIG_XENCAP
	((uint32_t *)data.dptr)[3] = node->num_caps;
#endif
#ifdef CONFIG_XENCAP
	p = data.dptr + 4 * sizeof(uint32_t);
#else
	p = data.dptr + 3 * sizeof(uint32_t);
#endif

	memcpy(p, node->perms, node->num_perms*sizeof(node->perms[0]));
	p += node->num_perms*sizeof(node->perms[0]);
#ifdef CONFIG_XENCAP
	memcpy(p, node->caps, node->num_caps*sizeof(node->caps[0]));
	p += node->num_caps*sizeof(node->caps[0]);
#endif
	memcpy(p, node->data, node->datalen);
	p += node->datalen;
	memcpy(p, node->children, node->childlen);
	//debug_xencap(key,data,node);
	
	if (domain_is_unprivileged(conn) && data.dsize >= quota_max_entry_size)
	{
#if CONFIG_XENCAP
		log("data.dsize >= quota_max_entry_size\n");
		goto error;
#endif
	}

	/* TDB should set errno, but doesn't even set ecode AFAICT. */
	if (tdb_store(tdb_context(conn), key, data, TDB_REPLACE) != 0) {
		corrupt(conn, "Write of %s failed", key.dptr);
		goto error;
	}
	return true;
 error:
	log("write_node: %s error.\n",node->name);
	errno = ENOSPC;
	return false;
}

#ifdef CONFIG_XENCAP
/* Mapping the ACL to Capabilities for a connection. */
static int caps_for_conn(struct connection *conn, 
		struct capability *caps, enum xs_perm_type perm, char *name)
{
	int ret = 1; /* success */
	int domid = get_domid(conn);
    xc_interface *xch;

	xch = xc_interface_open(0,0,0);

	if(xch == 0)
	{
		log("\nxc_interface_open failed to get xc_handle!\n");
		return 0;
	}

    /* Owners and tools get it all... */
	/* still need to identify the owners correctly .. 
	 * I'm commenting this for now to help me see
	 * if caps are present in domain 0 */
#if 0
    if (!domain_is_unprivileged(conn)) {              
		xc_interface_close(xch);
        return ret;
	}
#endif

	if (domid == -1)
		domid = 0;

	if (perm == XS_PERM_READ || perm == (XS_PERM_READ|XS_PERM_OWNER))
	{
		/* Read Capabilities
		 * Return true if we have read capabilities
		 * Or we have owner capabilities - But owner has both.
		 */
		if ( (xc_cap_check(xch, domid, &caps[0])) == 0 ) {
			log("caps_for_conn (conn:%p,domid:%d,perm:%d,name:%s) read cap does not exist!\n", conn, domid, perm, name);
			ret = 0;
		}
	}
	else if (perm == XS_PERM_WRITE || perm == (XS_PERM_WRITE|XS_PERM_OWNER))
	{
		/* Write Capabilities
		 * Return true if we have write capabilities
		 * Or we have owner capabilities - But owner has both.
		 */
		if ( (xc_cap_check(xch, domid, &caps[1])) == 0 ) {
			log("caps_for_conn (conn:%p,domid:%d,perm:%d,name:%s) write cap does not exist!\n", conn, domid, perm, name);
			ret = 0;
		}
	}
	else if (perm == XS_PERM_OWNER)
	{
		/* Read | Write Capabilities */
		/* Owner must have both. */
		if ( (xc_cap_check(xch, domid, &caps[0])) == 0 ||
			 (xc_cap_check(xch, domid, &caps[1])) == 0 ) {
			log("caps_for_conn (conn:%p,domid:%d,perm:%d,name:%s) either read or write cap does not exist!\n", conn, domid, perm, name);
			ret = 0;
		}
	}
	else
	{
		/* Return the default capability.
		   NOTE: DOUBT!!!
		   return success for now.
		 */
            
		log("caps_for_conn (conn:%p,domid:%d,perm:%d) WARRNING:return the default capability\n", conn, domid, perm);
		ret = 1;
	}

	xc_interface_close(xch);
	return ret;
}
#endif

static enum xs_perm_type perm_for_conn(struct connection *conn,
				   struct xs_permissions *perms,
				   unsigned int num)
{
	unsigned int i;
	enum xs_perm_type mask = XS_PERM_READ|XS_PERM_WRITE|XS_PERM_OWNER;

	if (!conn->can_write)
		mask &= ~XS_PERM_WRITE;

	/* Owners and tools get it all... */
	if (!domain_is_unprivileged(conn) || perms[0].id == conn->id
                || (conn->target && perms[0].id == conn->target->id))
		return (XS_PERM_READ|XS_PERM_WRITE|XS_PERM_OWNER) & mask;

	for (i = 1; i < num; i++)
		if (perms[i].id == conn->id
                        || (conn->target && perms[i].id == conn->target->id))
			return perms[i].perms & mask;

	return perms[0].perms & mask;
}

static char *get_parent(const char *node)
{
	char *slash = strrchr(node + 1, '/');
	if (!slash)
		return talloc_strdup(node, "/");
	return talloc_asprintf(node, "%.*s", (int)(slash - node), node);
}

/* What do parents say? */
static enum xs_perm_type ask_parents(struct connection *conn, const char *name)
{
	struct node *node;
#ifdef CONFIG_XENCAP
	enum xs_perm_type xs_perms = XS_PERM_NONE;
#endif	
	do {
		name = get_parent(name);
		node = read_node(conn, name);
		if (node)
			break;
	} while (!streq(name, "/"));

	/* No permission at root?  We're in trouble. */
	if (!node)
		corrupt(conn, "No permissions file at root");

#ifdef CONFIG_XENCAP
	if ( conn != NULL && conn->cap_flag ) {
		//trace("ask_parents: caps_for_conn: (conn:%p) name %s\n", conn, name);
		if (caps_for_conn(conn, node->caps, XS_PERM_READ, (char*) name))
			xs_perms = XS_PERM_READ;
		if (caps_for_conn(conn, node->caps, XS_PERM_WRITE,(char *) name))
			xs_perms |= XS_PERM_WRITE;
		return xs_perms;
	}
	else
#endif
		return perm_for_conn(conn, node->perms, node->num_perms);
}

/* We have a weird permissions system.  You can allow someone into a
 * specific node without allowing it in the parents.  If it's going to
 * fail, however, we don't want the errno to indicate any information
 * about the node. */
static int errno_from_parents(struct connection *conn, const char *node,
			      int errnum, enum xs_perm_type perm)
{
	/* We always tell them about memory failures. */
	if (errnum == ENOMEM)
		return errnum;

	if (ask_parents(conn, node) & perm)
		return errnum;
	return EACCES;
}

/* If it fails, returns NULL and sets errno. */
struct node *get_node(struct connection *conn,
		      const char *name,
		      enum xs_perm_type perm)
{
	struct node *node;

	if (!name || !is_valid_nodename(name)) {
		xprintf("Name is invalid!\n");
		errno = EINVAL;
		return NULL;
	}

	node = read_node(conn, name);
	log("GET_NODE: %s with PERM %d, read_node returned node %p\n",name, perm, node);
#ifdef CONFIG_XENCAP
	if ( conn != NULL && conn->cap_flag ) {
		if (node) {
			/* Note: This is to satisfy grant_caps */
			if (perm == XS_PERM_NONE)
				return node;
			log("caps_for_conn for node %s with perms %d\n",name, perm);
			if (!caps_for_conn(conn, node->caps, perm, (char*)name)) {
				errno = EACCES;
				node  = NULL;
			}
		}
	}
	else 
#endif		
	{
		/* If we don't have permission, we don't have node. */
		if (node) {
				if ((perm_for_conn(conn, node->perms, node->num_perms) & perm)
					!= perm) {
					log("get_node: ACL check failed.\n");
					errno = EACCES;
				node = NULL;
			}
		}
	}

	/* Clean up errno if they weren't supposed to know. */
	if (!node) 
		errno = errno_from_parents(conn, name, errno, perm);
	return node;
}

static struct buffered_data *new_buffer(void *ctx)
{
	struct buffered_data *data;

	data = talloc_zero(ctx, struct buffered_data);
	if (data == NULL)
		return NULL;
	
	data->inhdr = true;
	return data;
}

/* Return length of string (including nul) at this offset.
 * If there is no nul, returns 0 for failure.
 */
static unsigned int get_string(const struct buffered_data *data,
			       unsigned int offset)
{
	const char *nul;

	if (offset >= data->used)
		return 0;

	nul = memchr(data->buffer + offset, 0, data->used - offset);
	if (!nul)
		return 0;

	return nul - (data->buffer + offset) + 1;
}

/* Break input into vectors, return the number, fill in up to num of them.
 * Always returns the actual number of nuls in the input.  Stores the
 * positions of the starts of the nul-terminated strings in vec.
 * Callers who use this and then rely only on vec[] will
 * ignore any data after the final nul.
 */
unsigned int get_strings(struct buffered_data *data,
			 char *vec[], unsigned int num)
{
	unsigned int off, i, len;

	off = i = 0;
	while ((len = get_string(data, off)) != 0) {
		if (i < num)
			vec[i] = data->buffer + off;
		i++;
		off += len;
	}
	return i;
}

void send_reply(struct connection *conn, enum xsd_sockmsg_type type,
		const void *data, unsigned int len)
{
	struct buffered_data *bdata;

	/* Message is a child of the connection context for auto-cleanup. */
	bdata = new_buffer(conn);
	bdata->buffer = talloc_array(bdata, char, len);

	/* Echo request header in reply unless this is an async watch event. */
	if (type != XS_WATCH_EVENT) {
		memcpy(&bdata->hdr.msg, &conn->in->hdr.msg,
		       sizeof(struct xsd_sockmsg));
	} else {
		memset(&bdata->hdr.msg, 0, sizeof(struct xsd_sockmsg));
	}

	/* Update relevant header fields and fill in the message body. */
	bdata->hdr.msg.type = type;
	bdata->hdr.msg.len = len;
	memcpy(bdata->buffer, data, len);

	/* Queue for later transmission. */
	list_add_tail(&bdata->list, &conn->out_list);
}

/* Some routines (write, mkdir, etc) just need a non-error return */
void send_ack(struct connection *conn, enum xsd_sockmsg_type type)
{
	send_reply(conn, type, "OK", sizeof("OK"));
}

void send_error(struct connection *conn, int error)
{
	unsigned int i;

	for (i = 0; error != xsd_errors[i].errnum; i++) {
		if (i == ARRAY_SIZE(xsd_errors) - 1) {
			eprintf("xenstored: error %i untranslatable", error);
			i = 0; 	/* EINVAL */
			break;
		}
	}
	send_reply(conn, XS_ERROR, xsd_errors[i].errstring,
			  strlen(xsd_errors[i].errstring) + 1);
}

static bool valid_chars(const char *node)
{
	/* Nodes can have lots of crap. */
	return (strspn(node, 
		       "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		       "abcdefghijklmnopqrstuvwxyz"
		       "0123456789-/_@") == strlen(node));
}

bool is_valid_nodename(const char *node)
{
	/* Must start in /. */
	if (!strstarts(node, "/"))
		return false;

	/* Cannot end in / (unless it's just "/"). */
	if (strends(node, "/") && !streq(node, "/"))
		return false;

	/* No double //. */
	if (strstr(node, "//"))
		return false;

	if (strlen(node) > XENSTORE_ABS_PATH_MAX)
		return false;

	return valid_chars(node);
}

/* We expect one arg in the input: return NULL otherwise.
 * The payload must contain exactly one nul, at the end.
 */
static const char *onearg(struct buffered_data *in)
{
	if (!in->used || get_string(in, 0) != in->used)
		return NULL;
	return in->buffer;
}

#ifdef CONFIG_XENCAP
#if 0
static int grant_def_cap(xc_interface *xch,int *def_domains, int size, struct capability *cap, int def_size)
{
	int index;
	xprintf("grant_def_cap: Iterating the default domains list.\n");

	if (!get_def_domains(def_domains,size)) {
		for (index = 0; index < size; ++index)
		{
			/* Grant def cap. */
			if (def_domains[index] != -1)
			{
				if ( cap == NULL )
					continue;

				if (def_size == 1)
				{
					if (!xc_cap_grant(xch,def_domains[index],2,&cap[0],1))
					{					
						xprintf("grant_def_cap: Error in xc_cap_grant\n");
						return 1;
					}
				}

				if (def_size == 2)
				{
					if (!xc_cap_grant(xch,def_domains[index],2,&cap[0],2))
					{
						xprintf("grant_def_cap: Error in xc_cap_grant\n");				
						return 1;
					}

					if (!xc_cap_grant(xch,def_domains[index],2,&cap[1],2)) 
					{
						xprintf("grant_def_cap: Error in xc_cap_grant\n");
						return 1;
					}
				}

			}
		}
	}
	return 0;
}
#endif

/* Convert strings to capabilities. False if a problem. 
 * Permission scheme in xenstore is weird. Here are the rules -
  	1)	Domain 0 may read or write anywhere in the store, 
		regardless of permissions, and permissions are set 
		up by the tools in domain 0, or by Xenstored when 
		it first starts up. 
	2)	The first element in this list specifies the owner of the path.
	3)	The owner always has read and write access to their nodes. 
	4)  The example below, therefore, sets the permissions in the path 
		to be such that domain 0 (being privileged), dom1 (being the owner), 
		and domains dom2 and dom3 (being explicitly specified) can _all_ write 
		to the node. Any other domain can only read, as specified by the first 
		pair of 'read' and 'write' flags. 

		Example:
		xstransact.SetPermissions(path, { 'dom'   : dom1,
										  'read'  : True,
										  'write' : False },
										{ 'dom'   : dom2,
										  'read'  : True,
										  'write' : True },
										{ 'dom'   : dom3,
										  'read'  : True,
										  'write' : True })
*/
bool xs_strings_to_caps(unsigned int num, struct node *node, const char *strings)
{
	const char *p;
	char def_perm;
	char *end;
	unsigned int i;
    xc_interface *xch;
	unsigned int owner_domid,to_domid;
	struct capability list[2];
	struct capability def_cap[2];
	struct capability *null_cap = NULL;
	int def_domains[num];
	int flag = 0;	
	int def_size = 1;

	xch = xc_interface_open(0,0,0);

	if(xch == 0)
	{
		log("xs_strings_to_caps: xc_interface_open failed to get xc_handle!\n");
		goto out;
	}

	/* Since the owner domain has already created capabilities.
	   Skip over it but make sure it has them. */

	p = strings;
	def_perm = strings[0];
	p++;
	owner_domid = strtol(p, &end, 0);

	log("xs_strings_to_caps: owner_domid %d and node->name %s and node->caps[0]:0x%x node->caps[1]:0x%x\n",owner_domid,node->name,node->caps[0].magic,node->caps[1].magic);
	if ( node->caps == NULL )
	{
		log("xs_strings_to_caps: caps is NULL!\n");
		goto out;
	}

	if (!xc_cap_check(xch,owner_domid,&node->caps[0]))
	{
		list[0] = node->caps[0];
		if ((xc_cap_grant(xch,owner_domid,list,1)) == 1)
		{
			log("xs_strings_to_caps: Error in xc_cap_grant.\n");
			goto out;
		}
	}

	if (!xc_cap_check(xch,owner_domid,&node->caps[1]))
	{
        list[0] = node->caps[1];
        if ((xc_cap_grant(xch,owner_domid,list,1)) == 1)
        {
            log("xs_strings_to_caps: Error in xc_cap_grant.\n");
            goto out;
        }
	}

	/* Note the default permissions specified by set_perms. */
	if (def_perm == 'r')
		def_cap[0] = node->caps[0];
	if (def_perm == 'w')
		def_cap[0] = node->caps[1];
	if (def_perm == 'n')
		def_cap[0] = *null_cap;
	if (def_perm == 'b')
	{
		def_cap[0] = node->caps[0];
		def_cap[1] = node->caps[1];
		def_size = 2;
	}

	/* Grant the capabilities from owner to rest of the domains
	   specified in set_perms API. */

	for (p = end+1, i = 1; i < num; i++) {
		/* "r", "w", or "b" for both. */
		switch (*p) {
			case 'r':
				list[0] = node->caps[0];
				flag = 1;
				break;
			case 'w':
				list[0] = node->caps[1];
				flag = 1;
				break;
			case 'b':
				list[0] = node->caps[0];
				list[1] = node->caps[1];
				break;
			case 'n':
				break;
			default:
				errno = EINVAL;
				goto out;
		} 
		p++;
		to_domid = strtol(p, &end, 0);

		if (*end || !*p) {
			errno = EINVAL;
			goto out;
		}
	
		def_domains[i] = to_domid;	
		
		if (flag && to_domid != 0)
		{
			/* Read or Write Caps */
			if ((xc_cap_grant(xch,to_domid,list,1)) == 1)
			{
				log("xs_strings_to_caps: Error in xc_cap_grant.\n");
				goto out;
			}
		}
		else if (!flag && to_domid != 0)
		{
			/* Both the flags */
            if ((xc_cap_grant(xch,to_domid,&list[0],1)) == 1)
            {
                log("xs_strings_to_caps: Error in xc_cap_grant.\n");
				goto out;
            }
            if ((xc_cap_grant(xch,to_domid,&list[1],1)) == 1)
            {
                log("xs_strings_to_caps: Error in xc_cap_grant.\n");
                goto out;
            }
		}

		p = end + 1;
	}

    /* Invoke a function which will iterate through the 
	   given list of domains and grant default caps on them. */
	//if (!grant_def_cap(xch, def_domains, num, def_cap, def_size))
	//	log("grant_def_cap: Error\n");
		
	xc_interface_close(xch);
	return true;
out:
	xc_interface_close(xch);
	return false;
}
#endif


static char *perms_to_strings(const void *ctx,
			      struct xs_permissions *perms, unsigned int num,
			      unsigned int *len)
{
	unsigned int i;
	char *strings = NULL;
	char buffer[MAX_STRLEN(unsigned int) + 1];

	for (*len = 0, i = 0; i < num; i++) {
		if (!xs_perm_to_string(&perms[i], buffer, sizeof(buffer)))
			return NULL;

		strings = talloc_realloc(ctx, strings, char,
					 *len + strlen(buffer) + 1);
		strcpy(strings + *len, buffer);
		*len += strlen(buffer) + 1;
	}
	return strings;
}


char *canonicalize(struct connection *conn, const char *node)
{
	const char *prefix;

	if (!node || (node[0] == '/') || (node[0] == '@'))
		return (char *)node;
	prefix = get_implicit_path(conn);
	if (prefix)
		return talloc_asprintf(node, "%s/%s", prefix, node);
	return (char *)node;
}

bool check_event_node(const char *node)
{
	if (!node || !strstarts(node, "@")) {
		errno = EINVAL;
		return false;
	}
	return true;
}

static void send_directory(struct connection *conn, const char *name)
{
	struct node *node;

	name = canonicalize(conn, name);
	trace("name %s\n",name);			
	node = get_node(conn, name, XS_PERM_READ);
	trace("got back node %p\n",node);
	if (!node) {
		send_error(conn, errno);
		return;
	}

	send_reply(conn, XS_DIRECTORY, node->children, node->childlen);
}

static void do_read(struct connection *conn, const char *name)
{
	struct node *node;

	name = canonicalize(conn, name);
	node = get_node(conn, name, XS_PERM_READ);
	if (!node) {
		send_error(conn, errno);
		return;
	}

	send_reply(conn, XS_READ, node->data, node->datalen);
}

static void delete_node_single(struct connection *conn, struct node *node)
{
	TDB_DATA key;

	key.dptr = (void *)node->name;
	key.dsize = strlen(node->name);

	if (tdb_delete(tdb_context(conn), key) != 0) {
		corrupt(conn, "Could not delete '%s'", node->name);
		return;
	}
	domain_entry_dec(conn, node);
}

/* Must not be / */
static char *basename(const char *name)
{
	return strrchr(name, '/') + 1;
}

#ifdef CONFIG_XENCAP
/* For a given node, iterate through the domain list.
   Find out which domains has the reqd capabilities 
   to access this node.
 */  
static char *caps_to_strings(struct node *node, unsigned int *len, int grant_flag)
{
	xc_dominfo_t dominfo;
	//struct domain *domain, *tmp;
	struct xs_permissions perm;
	char *strings = NULL;
	char buffer[MAX_STRLEN(unsigned int) + 1];
	xc_interface *xch;
	const void *ctx = node;
	static LIST_HEAD(domains);
	int domid, n = 0;
	*len = 0;

	xch = xc_interface_open(0,0,0);

	if(xch == 0)
	{
		printf("xc_interface_open failed to get xc_handle!\n");
		return NULL;
	}

	//list_for_each_entry_safe(domain, tmp, &domains, list) {
	for (domid = 0; domid < 10; ++domid) {	
		if (xc_domain_getinfo(xch, domid, 1,
				      &dominfo) == 1 &&
		    dominfo.domid == domid) {
			/* Continue if this is not in cap mode. */
			if (!dominfo.cap_flag)
			{
				log("Domid %d : Dominfo.cap_flag %d\n",domid,dominfo.cap_flag);
				continue;
			}
		}
		else
		{
			log("caps_to_strings: Domid %d not found.\n",domid);
			continue;
		}


		/* Init the perm struct. */
		perm.id = -1;
		perm.perms = XS_PERM_NONE;

		n = 0;

		if (node->caps == NULL && domid != 0)
			log("node: %s node->caps is NULL for domid %d\n",node->name,domid);

		/* Check if this domain has the read capability. */
		if (xc_cap_check(xch, domid, &node->caps[0]))
		{
			perm.id = domid;
			perm.perms = XS_PERM_READ;			

			/* If grant_flag is set then we are using this
		   	routine to simply grant caps to other domains. */
			if (grant_flag)
			{
				n = sprintf(buffer,"%d",domid);
			}	
		
		}

		/* Check if this domain has the write capability. */
		if (xc_cap_check(xch, domid, &node->caps[1]))
		{
			/* If a node does not have read cap then we don't want
			   to write the same domid again. */
			if (grant_flag && perm.id == -1)
			{
				sprintf(buffer+n,"%d",domid);
			}
			perm.id = domid;
			perm.perms |= XS_PERM_WRITE;
		}

		if (!grant_flag && perm.id != -1)
		{
			if (!xs_perm_to_string(&perm, buffer, sizeof(buffer)))
			{
				log("xs_perm_to_string: got back NULL\n");
				return NULL;
			}
	        strings = talloc_realloc(ctx, strings, char,
                                                *len + strlen(buffer) + 1);
    	    strcpy(strings + *len, buffer);
        	*len += strlen(buffer) + 1;
		}
		else if (grant_flag && perm.id != -1) { 
			strings = talloc_realloc(ctx, strings, char,
												*len + strlen(buffer) + 1);
			strcpy(strings + *len, buffer);
			*len += strlen(buffer) + 1;		
		}
	}

	return strings;
}
#endif

#if 0
static int ctoi(char c)
{
	int x=0;
 	for(x=0; x<10; x++)
		if(c==((char)48+x)) return x;
		
	return -1;
}
#endif

static struct node *construct_node(struct connection *conn, const char *name)
{
	const char *base;
	unsigned int baselen;
	struct node *parent, *node;
	char *children, *parentname = get_parent(name);
	xc_interface *xch;
	struct capability list[2];
#ifdef CONFIG_XENCAP
	int domid = get_domid(conn);
	xch = xc_interface_open(0,0,0);

	if(xch == 0)
	{
		log("construct_node: xc_interface_open failed to get xc_handle!\n");
		return false;
	}
#endif

	/* If parent doesn't exist, create it. */
	parent = read_node(conn, parentname);
	if (!parent)
		parent = construct_node(conn, parentname);
	if (!parent)
#ifdef CONFIG_XENCAP
		goto out;
#else 
		return NULL;
#endif

	if (domain_entry(conn) >= quota_nb_entry_per_domain)
#ifdef CONFIG_XENCAP
		goto out;
#else
		return NULL;
#endif

	/* Add child to parent. */
	base = basename(name);
	baselen = strlen(base) + 1;
	children = talloc_array(name, char, parent->childlen + baselen);
	memcpy(children, parent->children, parent->childlen);
	memcpy(children + parent->childlen, base, baselen);
	parent->children = children;
	parent->childlen += baselen;

	/* Allocate node */
	node = talloc(name, struct node);
	node->tdb = tdb_context(conn);
	node->name = talloc_strdup(node, name);

	/* Inherit permissions, except unprivileged domains own what they create */
	node->num_perms = parent->num_perms;
	node->perms = talloc_memdup(node, parent->perms,
				    node->num_perms * sizeof(node->perms[0]));
#ifdef CONFIG_XENCAP
	/* It was decided that every node by default gets capabilities.
	 * Therefore, capability is created here and granted based on
	 * the domids. dom0 gets it and grants it to other domains.
	 */
	node->num_caps = 2;
	node->caps = talloc_array(node, struct capability, 2);

	log("Target of conn: %p, Node: %s, conn->id:%d conn->domain:%p\n",conn->target, node->name, conn->id, conn->domain);

	if (xc_cap_create(xch, &node->caps[0]))
	{
		xc_interface_close(xch);
		log("construct_node: Error in xc_cap_create.\n");
		// should return err
	}

	if (xc_cap_create(xch, &node->caps[1]))
	{
		xc_interface_close(xch);
		log("construct_node: Error in xc_cap_create.\n");
		// should return err
	}

	/* Grant read/write caps to the creating domain. 
	 * Note: If a guest domain is creating this node,
	 * we grant it both read/write caps since it must
	 * be the owner of this node.
	 */
	if (domid != -1 && domid != 0 && conn->cap_flag)
	{
		log("Construct_node: Guest domain: %d is making an entry in Xenstore\n",domid);
		list[0] = node->caps[0];
		log("construct_node: Granting read cap to domain:%d for node: %s\n",domid,node->name);			
		if (xc_cap_grant(xch, domid, list, 1) == 1)
			log("construct_node: Error in xc_cap_grant.\n");

		list[0] = node->caps[1]; 
		log("construct_node: Granting write cap to domain:%d for node: %s\n",domid,node->name);
		if (xc_cap_grant(xch, domid, list, 1) == 1)
			log("construct_node: Error in xc_cap_grant.\n");
	}
#endif
	if (domain_is_unprivileged(conn))
		node->perms[0].id = conn->id;

	/* No children, no data */
	node->children = node->data = NULL;
	node->childlen = node->datalen = 0;
	node->parent = parent;
	domain_entry_inc(conn, node);
#ifdef CONFIG_XENCAP
	xc_interface_close(xch);
#endif
	return node;
#ifdef CONFIG_XENCAP	
out:
	xc_interface_close(xch);
	return NULL;
#endif
}

static int destroy_node(void *_node)
{
	struct node *node = _node;
	TDB_DATA key;

	if (streq(node->name, "/"))
		corrupt(NULL, "Destroying root node!");

	key.dptr = (void *)node->name;
	key.dsize = strlen(node->name);

	tdb_delete(node->tdb, key);
	return 0;
}

static struct node *create_node(struct connection *conn, 
				const char *name,
				void *data, unsigned int datalen)
{
	struct node *node, *i;
	node = construct_node(conn, name);
	if (!node)
		return NULL;

	node->data = data;
	node->datalen = datalen;

	/* We write out the nodes down, setting destructor in case
	 * something goes wrong. */
	for (i = node; i; i = i->parent) {
		if (!write_node(conn, i)) {
			domain_entry_dec(conn, i);
			return NULL;
		}
		talloc_set_destructor(i, destroy_node);
	}

	/* OK, now remove destructors so they stay around */
	for (i = node; i; i = i->parent)
		talloc_set_destructor(i, NULL);
	return node;
}

/* path, data... */
static void do_write(struct connection *conn, struct buffered_data *in)
{
	unsigned int offset, datalen;
	struct node *node;
	char *vec[1] = { NULL }; /* gcc4 + -W + -Werror fucks code. */
	char *name;

	/* Extra "strings" can be created by binary data. */
	if (get_strings(in, vec, ARRAY_SIZE(vec)) < ARRAY_SIZE(vec)) {
		send_error(conn, EINVAL);
		return;
	}

	offset = strlen(vec[0]) + 1;
	datalen = in->used - offset;

	name = canonicalize(conn, vec[0]);
	node = get_node(conn, name, XS_PERM_WRITE);
	if (!node) {
		/* No permissions, invalid input? */
		if (errno != ENOENT) {
			log("do_write: node cannot be created, errno != ENOENT for %s\n",name);
			send_error(conn, errno);
			return;
		}
		node = create_node(conn, name, in->buffer + offset, datalen);
		if (!node) {
			log("do_write: error in create_node\n");
			send_error(conn, errno);
			return;
		}
	} else {
		node->data = in->buffer + offset;
		node->datalen = datalen;
		log("writing node: %s and conn->id: %d\n",node->name, conn->id);
		if (!write_node(conn, node)){
			log("do_write: error in write_node\n");
			send_error(conn, errno);
			return;
		}
	}

	add_change_node(conn->transaction, name, false);
	fire_watches(conn, name, false);
	send_ack(conn, XS_WRITE);
}

static void do_mkdir(struct connection *conn, const char *name)
{
	struct node *node;

	name = canonicalize(conn, name);
	node = get_node(conn, name, XS_PERM_WRITE);

	/* If it already exists, fine. */
	if (!node) {
		/* No permissions? */
		if (errno != ENOENT) {
			send_error(conn, errno);
			return;
		}
		node = create_node(conn, name, NULL, 0);
		if (!node) {
#ifdef CONFIG_XENCAP
			log("do_mkdir: new node for %s failed!\n",name);
#endif
			send_error(conn, errno);
			return;
		}
		add_change_node(conn->transaction, name, false);
		fire_watches(conn, name, false);
	}
	send_ack(conn, XS_MKDIR);
}

static void delete_node(struct connection *conn, struct node *node)
{
	unsigned int i;

	/* Delete self, then delete children.  If we crash, then the worst
	   that can happen is the children will continue to take up space, but
	   will otherwise be unreachable. */
	delete_node_single(conn, node);

	/* Delete children, too. */
	for (i = 0; i < node->childlen; i += strlen(node->children+i) + 1) {
		struct node *child;

		child = read_node(conn, 
				  talloc_asprintf(node, "%s/%s", node->name,
						  node->children + i));
		if (child) {
			delete_node(conn, child);
		}
		else {
			trace("delete_node: No child '%s/%s' found!\n",
			      node->name, node->children + i);
			/* Skip it, we've already deleted the parent. */
		}
	}
}


/* Delete memory using memmove. */
static void memdel(void *mem, unsigned off, unsigned len, unsigned total)
{
	memmove(mem + off, mem + off + len, total - off - len);
}


static bool remove_child_entry(struct connection *conn, struct node *node,
			       size_t offset)
{
	size_t childlen = strlen(node->children + offset);
	memdel(node->children, offset, childlen + 1, node->childlen);
	node->childlen -= childlen + 1;
	return write_node(conn, node);
}


static bool delete_child(struct connection *conn,
			 struct node *node, const char *childname)
{
	unsigned int i;

	for (i = 0; i < node->childlen; i += strlen(node->children+i) + 1) {
		if (streq(node->children+i, childname)) {
			return remove_child_entry(conn, node, i);
		}
	}
	corrupt(conn, "Can't find child '%s' in %s", childname, node->name);
	return false;
}


static int _rm(struct connection *conn, struct node *node, const char *name)
{
	/* Delete from parent first, then if we crash, the worst that can
	   happen is the child will continue to take up space, but will
	   otherwise be unreachable. */
	struct node *parent = read_node(conn, get_parent(name));
	if (!parent) {
		send_error(conn, EINVAL);
		return 0;
	}

	if (!delete_child(conn, parent, basename(name))) {
		send_error(conn, EINVAL);
		return 0;
	}

	delete_node(conn, node);
	return 1;
}


static void internal_rm(const char *name)
{
	char *tname = talloc_strdup(NULL, name);
	struct node *node = read_node(NULL, tname);
	if (node)
		_rm(NULL, node, tname);
	talloc_free(node);
	talloc_free(tname);
}


static void do_rm(struct connection *conn, const char *name)
{
	struct node *node;

	name = canonicalize(conn, name);
	node = get_node(conn, name, XS_PERM_WRITE);
	if (!node) {
		/* Didn't exist already?  Fine, if parent exists. */
		if (errno == ENOENT) {
			node = read_node(conn, get_parent(name));			
			if (node) {
				send_ack(conn, XS_RM);
				return;
			}
			/* Restore errno, just in case. */
			errno = ENOENT;
		}
		send_error(conn, errno);
		return;
	}

	if (streq(name, "/")) {
		log("do_rm: remove / is invalid\n");
		send_error(conn, EINVAL);
		return;
	}

	if (_rm(conn, node, name)) {
		add_change_node(conn->transaction, name, true);
		fire_watches(conn, name, true);
		send_ack(conn, XS_RM);
	}
}

static void do_get_perms(struct connection *conn, const char *name)
{
	struct node *node;
	char *strings = NULL;
	unsigned int len = 0;

	name = canonicalize(conn, name);
	node = get_node(conn, name, XS_PERM_READ);
	if (!node) {
		send_error(conn, errno);
		return;
	}

#ifdef CONFIG_XENCAP
	if ( conn != NULL && conn->cap_flag )
		strings = caps_to_strings(node, &len, 0);
	else
#endif
		strings = perms_to_strings(node, node->perms, node->num_perms, &len);
	if (!strings)
		send_error(conn, errno);
	else
		send_reply(conn, XS_GET_PERMS, strings, len);
}

static void do_set_perms(struct connection *conn, struct buffered_data *in)
{
	unsigned int num;
	struct xs_permissions *perms;
	char *name, *permstr;
	struct node *node;
#ifdef CONFIG_XENCAP
    xc_interface *xch;
    //int domid = get_domid(conn);
#endif

	num = xs_count_strings(in->buffer, in->used);
	if (num < 2) {
		log("do_set_perms num is < 2\n");
		send_error(conn, EINVAL);
		return;
	}

	/* First arg is node name. */
	name = canonicalize(conn, in->buffer);
	permstr = in->buffer + strlen(in->buffer) + 1;
	num--;

	log("do_set_perms for %s with perms %s\n",name,permstr);
	/* We must own node to do this (tools can do this too). */
	node = get_node(conn, name, XS_PERM_WRITE|XS_PERM_OWNER);
	if (!node) {
		send_error(conn, errno);
		return;
	}
#ifdef CONFIG_XENCAP
    if ( conn != NULL && conn->cap_flag )
    {
		if ( node->num_caps == 0 ) 
			node->num_caps = 2;
		if ( node->caps == NULL )	
			node->caps = talloc_array(node, struct capability, 2);

		xch = xc_interface_open(0,0,0);

		if(xch == 0)
		{
			log("xc_interface_open failed to get xc_handle!\n");
			return;
		}

		if (!xs_strings_to_caps(num, node, permstr)) {
			log("do_set_perms: xs_strings_to_caps failed.\n");
			send_error(conn,errno);
			xc_interface_close(xch);
			return;
		}
		
		xc_interface_close(xch);
		/* Note: Understand this why they do this ! */
		domain_entry_dec(conn, node);
		domain_entry_inc(conn, node);
	}
	else
	{
#endif
		perms = talloc_array(node, struct xs_permissions, num);
		if (!xs_strings_to_perms(perms, num, permstr)) {
				send_error(conn, errno);
				return;
		}
		/* Unprivileged domains may not change the owner. */
		if (domain_is_unprivileged(conn) &&
			perms[0].id != node->perms[0].id) {
			send_error(conn, EPERM);
			return;
		}

		domain_entry_dec(conn, node);
		node->perms = perms;
		node->num_perms = num;
		domain_entry_inc(conn, node);
	}

	if (!write_node(conn, node)) {
		send_error(conn, errno);
		return;
	}
	
	add_change_node(conn->transaction, name, false);
	fire_watches(conn, name, false);
	send_ack(conn, XS_SET_PERMS);
}

#ifdef CONFIG_XENCAP
/* Get capabilities on a node. */
static void do_get_caps(struct connection *conn, const char *name)
{
	struct node *node;
	char *path;
	struct capability *caps = NULL;

    path = canonicalize(conn, name); 
	node = get_node(conn, path, XS_PERM_NONE);
	if (!node) {
		send_error(conn, errno);
		return;
	}

	caps = talloc_array(node,struct capability,2);
	
	if (&node->caps[0])
		caps[0] = node->caps[0];

	if (&node->caps[1])
		caps[1] = node->caps[1];

	if (caps == NULL) { 
		send_reply(conn, XS_GET_CAPS, NULL, 0);
		return;
	}		
	send_reply(conn, XS_GET_CAPS, caps, 2*sizeof(caps[0]));
	talloc_free(caps);
}
#endif

static void do_debug(struct connection *conn, struct buffered_data *in)
{
	int num;

	if (conn->id != 0) {
		send_error(conn, EACCES);
		return;
	}

	num = xs_count_strings(in->buffer, in->used);

	if (streq(in->buffer, "print")) {
		if (num < 2) {
			send_error(conn, EINVAL);
			return;
		}
		xprintf("debug: %s", in->buffer + get_string(in, 0));
	}

	if (streq(in->buffer, "check"))
		check_store();

	send_ack(conn, XS_DEBUG);
}

/* Process "in" for conn: "in" will vanish after this conversation, so
 * we can talloc off it for temporary variables.  May free "conn".
 */
static void process_message(struct connection *conn, struct buffered_data *in)
{
	struct transaction *trans;

	trans = transaction_lookup(conn, in->hdr.msg.tx_id);
	if (IS_ERR(trans)) {
		send_error(conn, -PTR_ERR(trans));
		return;
	}

	assert(conn->transaction == NULL);
	conn->transaction = trans;

	switch (in->hdr.msg.type) {
	case XS_DIRECTORY:
		send_directory(conn, onearg(in));
		break;

	case XS_READ:
		do_read(conn, onearg(in));
		break;

	case XS_WRITE:
		do_write(conn, in);
		break;

	case XS_MKDIR:
		do_mkdir(conn, onearg(in));
		break;

	case XS_RM:
		do_rm(conn, onearg(in));
		break;

	case XS_GET_PERMS:
		do_get_perms(conn, onearg(in));
		break;

	case XS_SET_PERMS:
		do_set_perms(conn, in);
		break;
#ifdef CONFIG_XENCAP
	case XS_GET_CAPS:
		do_get_caps(conn, onearg(in));
		break;
#endif
	case XS_DEBUG:
		do_debug(conn, in);
		break;

	case XS_WATCH:
		do_watch(conn, in);
		break;

	case XS_UNWATCH:
		do_unwatch(conn, in);
		break;

	case XS_TRANSACTION_START:
		do_transaction_start(conn, in);
		break;

	case XS_TRANSACTION_END:
		do_transaction_end(conn, onearg(in));
		break;

	case XS_INTRODUCE:
		do_introduce(conn, in);
		break;

	case XS_IS_DOMAIN_INTRODUCED:
		do_is_domain_introduced(conn, onearg(in));
		break;

	case XS_RELEASE:
		do_release(conn, onearg(in));
		break;

	case XS_GET_DOMAIN_PATH:
		do_get_domain_path(conn, onearg(in));
		break;

	case XS_RESUME:
		do_resume(conn, onearg(in));
		break;

	case XS_SET_TARGET:
		do_set_target(conn, in);
		break;

	case XS_RESET_WATCHES:
		do_reset_watches(conn);
		break;

	default:
		eprintf("Client unknown operation %i", in->hdr.msg.type);
		send_error(conn, ENOSYS);
		break;
	}

	conn->transaction = NULL;
}

static void consider_message(struct connection *conn)
{
	if (verbose)
		xprintf("Got message %s len %i from %p\n",
			sockmsg_string(conn->in->hdr.msg.type),
			conn->in->hdr.msg.len, conn);

	process_message(conn, conn->in);

	talloc_free(conn->in);
	conn->in = new_buffer(conn);
}

/* Errors in reading or allocating here mean we get out of sync, so we
 * drop the whole client connection. */
static void handle_input(struct connection *conn)
{
	int bytes;
	struct buffered_data *in = conn->in;

	/* Not finished header yet? */
	if (in->inhdr) {
		bytes = conn->read(conn, in->hdr.raw + in->used,
				   sizeof(in->hdr) - in->used);
		if (bytes < 0)
			goto bad_client;
		in->used += bytes;
		if (in->used != sizeof(in->hdr))
			return;

		if (in->hdr.msg.len > XENSTORE_PAYLOAD_MAX) {
			syslog(LOG_ERR, "Client tried to feed us %i",
			       in->hdr.msg.len);
			goto bad_client;
		}

		in->buffer = talloc_array(in, char, in->hdr.msg.len);
		if (!in->buffer)
			goto bad_client;
		in->used = 0;
		in->inhdr = false;
	}

	bytes = conn->read(conn, in->buffer + in->used,
			   in->hdr.msg.len - in->used);
	if (bytes < 0)
		goto bad_client;

	in->used += bytes;
	if (in->used != in->hdr.msg.len)
		return;

	trace_io(conn, in, 0);
	consider_message(conn);
	return;

bad_client:
	/* Kill it. */
	talloc_free(conn);
}

static void handle_output(struct connection *conn)
{
	if (!write_messages(conn))
		talloc_free(conn);
}

struct connection *new_connection(connwritefn_t *write, connreadfn_t *read)
{
	struct connection *new;

	new = talloc_zero(talloc_autofree_context(), struct connection);
	if (!new)
		return NULL;

	new->fd = -1;
	new->write = write;
	new->read = read;
	new->can_write = true;
	new->transaction_started = 0;
#ifdef CONFIG_XENCAP
	/* Note: The problem here is to identify if this connection
	   is a part of the same transaction started by a domain
	   which is in cap mode. */

	/* Heres a thought: Since these connections mostly don't have
	   any domain associated with it, lets simply grant them acess
	   to nodes ? */
	new->cap_flag = 1;
#endif
	INIT_LIST_HEAD(&new->out_list);
	INIT_LIST_HEAD(&new->watches);
	INIT_LIST_HEAD(&new->transaction_list);

	new->in = new_buffer(new);
	if (new->in == NULL) {
		talloc_free(new);
		return NULL;
	}

	list_add_tail(&new->list, &connections);
	talloc_set_destructor(new, destroy_conn);
	trace_create(new, "connection");
	return new;
}

#ifdef NO_SOCKETS
static void accept_connection(int sock, bool canwrite)
{
}
#else
static int writefd(struct connection *conn, const void *data, unsigned int len)
{
	int rc;

	while ((rc = write(conn->fd, data, len)) < 0) {
		if (errno == EAGAIN) {
			rc = 0;
			break;
		}
		if (errno != EINTR)
			break;
	}

	return rc;
}

static int readfd(struct connection *conn, void *data, unsigned int len)
{
	int rc;

	while ((rc = read(conn->fd, data, len)) < 0) {
		if (errno == EAGAIN) {
			rc = 0;
			break;
		}
		if (errno != EINTR)
			break;
	}

	/* Reading zero length means we're done with this connection. */
	if ((rc == 0) && (len != 0)) {
		errno = EBADF;
		rc = -1;
	}

	return rc;
}

static void accept_connection(int sock, bool canwrite)
{
	int fd;
	struct connection *conn;

	fd = accept(sock, NULL, NULL);
	if (fd < 0)
		return;

	conn = new_connection(writefd, readfd);
	if (conn) {
		conn->fd = fd;
		conn->can_write = canwrite;
	} else
		close(fd);
}
#endif

static int tdb_flags;

/* We create initial nodes manually. */
static void manual_node(const char *name, const char *child)
{
	struct node *node;
	struct xs_permissions perms = { .id = 0, .perms = XS_PERM_NONE };
#ifdef CONFIG_XENCAP
	xc_interface *xch;
#endif
	
	node = talloc_zero(NULL, struct node);
	node->name = name;
	node->perms = &perms;
	node->num_perms = 1;
#ifdef CONFIG_XENCAP
	/* Not sure about this */
	node->caps = talloc_array(node,struct capability,2);
	node->num_caps = 2;
	xch = xc_interface_open(0,0,0);
    if (xc_cap_create(xch,&node->caps[0]))
    {
        log("manual_node: Error in xc_cap_create.\n");
        xc_interface_close(xch);
    }

    if (xc_cap_create(xch, &node->caps[1]))
    {
        log("manual_node: Error in xc_cap_create.\n");
        xc_interface_close(xch);
    }
	xc_interface_close(xch);
#endif	
	node->children = (char *)child;
	if (child)
		node->childlen = strlen(child) + 1;

	if (!write_node(NULL, node))
		barf_perror("Could not create initial node %s", name);
	talloc_free(node);
}

static void setup_structure(void)
{
	char *tdbname;
	tdbname = talloc_strdup(talloc_autofree_context(), xs_daemon_tdb());

	if (!(tdb_flags & TDB_INTERNAL))
		tdb_ctx = tdb_open(tdbname, 0, tdb_flags, O_RDWR, 0);

	if (tdb_ctx) {
		/* XXX When we make xenstored able to restart, this will have
		   to become cleverer, checking for existing domains and not
		   removing the corresponding entries, but for now xenstored
		   cannot be restarted without losing all the registered
		   watches, which breaks all the backend drivers anyway.  We
		   can therefore get away with just clearing /local and
		   expecting Xend to put the appropriate entries back in.

		   When this change is made it is important to note that
		   dom0's entries must be cleaned up on reboot _before_ this
		   daemon starts, otherwise the backend drivers and dom0's
		   balloon driver will pick up stale entries.  In the case of
		   the balloon driver, this can be fatal.
		*/
		char *tlocal = talloc_strdup(NULL, "/local");

		check_store();

		if (remove_local) {
			internal_rm("/local");
			create_node(NULL, tlocal, NULL, 0);

			check_store();
		}

		talloc_free(tlocal);
	}
	else {
		tdb_ctx = tdb_open(tdbname, 7919, tdb_flags, O_RDWR|O_CREAT,
				   0640);
		if (!tdb_ctx)
			barf_perror("Could not create tdb file %s", tdbname);

		manual_node("/", "tool");
		manual_node("/tool", "xenstored");
		manual_node("/tool/xenstored", NULL);

		check_store();
	}
}


static unsigned int hash_from_key_fn(void *k)
{
	char *str = k;
	unsigned int hash = 5381;
	char c;

	while ((c = *str++))
		hash = ((hash << 5) + hash) + (unsigned int)c;

	return hash;
}


static int keys_equal_fn(void *key1, void *key2)
{
	return 0 == strcmp((char *)key1, (char *)key2);
}


static char *child_name(const char *s1, const char *s2)
{
	if (strcmp(s1, "/")) {
		return talloc_asprintf(NULL, "%s/%s", s1, s2);
	}
	else {
		return talloc_asprintf(NULL, "/%s", s2);
	}
}


static void remember_string(struct hashtable *hash, const char *str)
{
	char *k = malloc(strlen(str) + 1);
	strcpy(k, str);
	hashtable_insert(hash, k, (void *)1);
}


/**
 * A node has a children field that names the children of the node, separated
 * by NULs.  We check whether there are entries in there that are duplicated
 * (and if so, delete the second one), and whether there are any that do not
 * have a corresponding child node (and if so, delete them).  Each valid child
 * is then recursively checked.
 *
 * No deleting is performed if the recovery flag is cleared (i.e. -R was
 * passed on the command line).
 *
 * As we go, we record each node in the given reachable hashtable.  These
 * entries will be used later in clean_store.
 */
static void check_store_(const char *name, struct hashtable *reachable)
{
	struct node *node = read_node(NULL, name);

	if (node) {
		size_t i = 0;

		struct hashtable * children =
			create_hashtable(16, hash_from_key_fn, keys_equal_fn);

		remember_string(reachable, name);

		while (i < node->childlen) {
			size_t childlen = strlen(node->children + i);
			char * childname = child_name(node->name,
						      node->children + i);
			struct node *childnode = read_node(NULL, childname);
			
			if (childnode) {
				if (hashtable_search(children, childname)) {
					log("check_store: '%s' is duplicated!",
					    childname);

					if (recovery) {
						remove_child_entry(NULL, node,
								   i);
						i -= childlen + 1;
					}
				}
				else {
					remember_string(children, childname);
					check_store_(childname, reachable);
				}
			}
			else {
				log("check_store: No child '%s' found!\n",
				    childname);

				if (recovery) {
					remove_child_entry(NULL, node, i);
					i -= childlen + 1;
				}
			}

			talloc_free(childnode);
			talloc_free(childname);
			i += childlen + 1;
		}

		hashtable_destroy(children, 0 /* Don't free values (they are
						 all (void *)1) */);
		talloc_free(node);
	}
	else {
		/* Impossible, because no database should ever be without the
		   root, and otherwise, we've just checked in our caller
		   (which made a recursive call to get here). */
		   
		log("check_store: No child '%s' found: impossible!", name);
	}
}


/**
 * Helper to clean_store below.
 */
static int clean_store_(TDB_CONTEXT *tdb, TDB_DATA key, TDB_DATA val,
			void *private)
{
	struct hashtable *reachable = private;
	char * name = talloc_strndup(NULL, key.dptr, key.dsize);

	if (!hashtable_search(reachable, name)) {
		log("clean_store: '%s' is orphaned!", name);
		if (recovery) {
			tdb_delete(tdb, key);
		}
	}

	talloc_free(name);

	return 0;
}


/**
 * Given the list of reachable nodes, iterate over the whole store, and
 * remove any that were not reached.
 */
static void clean_store(struct hashtable *reachable)
{
	tdb_traverse(tdb_ctx, &clean_store_, reachable);
}


static void check_store(void)
{
	char * root = talloc_strdup(NULL, "/");
	struct hashtable * reachable =
		create_hashtable(16, hash_from_key_fn, keys_equal_fn);
 
	log("Checking store ...");
	check_store_(root, reachable);
	clean_store(reachable);
	log("Checking store complete.");

	hashtable_destroy(reachable, 0 /* Don't free values (they are all
					  (void *)1) */);
	talloc_free(root);
}


/* Something is horribly wrong: check the store. */
static void corrupt(struct connection *conn, const char *fmt, ...)
{
	va_list arglist;
	char *str;
	int saved_errno = errno;

	va_start(arglist, fmt);
	str = talloc_vasprintf(NULL, fmt, arglist);
	va_end(arglist);

	log("corruption detected by connection %i: err %s: %s",
	    conn ? (int)conn->id : -1, strerror(saved_errno), str);

	check_store();
}


#ifdef NO_SOCKETS
static void init_sockets(int **psock, int **pro_sock)
{
	static int minus_one = -1;
	*psock = *pro_sock = &minus_one;
}
#else
static int destroy_fd(void *_fd)
{
	int *fd = _fd;
	close(*fd);
	return 0;
}

static void init_sockets(int **psock, int **pro_sock)
{
	struct sockaddr_un addr;
	int *sock, *ro_sock;
	/* Create sockets for them to listen to. */
	*psock = sock = talloc(talloc_autofree_context(), int);
	*sock = socket(PF_UNIX, SOCK_STREAM, 0);
	if (*sock < 0)
		barf_perror("Could not create socket");
	*pro_sock = ro_sock = talloc(talloc_autofree_context(), int);
	*ro_sock = socket(PF_UNIX, SOCK_STREAM, 0);
	if (*ro_sock < 0)
		barf_perror("Could not create socket");
	talloc_set_destructor(sock, destroy_fd);
	talloc_set_destructor(ro_sock, destroy_fd);

	/* FIXME: Be more sophisticated, don't mug running daemon. */
	unlink(xs_daemon_socket());
	unlink(xs_daemon_socket_ro());

	addr.sun_family = AF_UNIX;
	strcpy(addr.sun_path, xs_daemon_socket());
	if (bind(*sock, (struct sockaddr *)&addr, sizeof(addr)) != 0)
		barf_perror("Could not bind socket to %s", xs_daemon_socket());
	strcpy(addr.sun_path, xs_daemon_socket_ro());
	if (bind(*ro_sock, (struct sockaddr *)&addr, sizeof(addr)) != 0)
		barf_perror("Could not bind socket to %s",
			    xs_daemon_socket_ro());
	if (chmod(xs_daemon_socket(), 0600) != 0
	    || chmod(xs_daemon_socket_ro(), 0660) != 0)
		barf_perror("Could not chmod sockets");

	if (listen(*sock, 1) != 0
	    || listen(*ro_sock, 1) != 0)
		barf_perror("Could not listen on sockets");


}
#endif

static void usage(void)
{
	fprintf(stderr,
"Usage:\n"
"\n"
"  xenstored <options>\n"
"\n"
"where options may include:\n"
"\n"
"  --no-domain-init    to state that xenstored should not initialise dom0,\n"
"  --pid-file <file>   giving a file for the daemon's pid to be written,\n"
"  --help              to output this message,\n"
"  --no-fork           to request that the daemon does not fork,\n"
"  --output-pid        to request that the pid of the daemon is output,\n"
"  --trace-file <file> giving the file for logging, and\n"
"  --entry-nb <nb>     limit the number of entries per domain,\n"
"  --entry-size <size> limit the size of entry per domain, and\n"
"  --watch-nb <nb>     limit the number of watches per domain,\n"
"  --transaction <nb>  limit the number of transaction allowed per domain,\n"
"  --no-recovery       to request that no recovery should be attempted when\n"
"                      the store is corrupted (debug only),\n"
"  --internal-db       store database in memory, not on disk\n"
"  --preserve-local    to request that /local is preserved on start-up,\n"
"  --verbose           to request verbose execution.\n");
}


static struct option options[] = {
	{ "no-domain-init", 0, NULL, 'D' },
	{ "entry-nb", 1, NULL, 'E' },
	{ "pid-file", 1, NULL, 'F' },
	{ "event", 1, NULL, 'e' },
	{ "help", 0, NULL, 'H' },
	{ "no-fork", 0, NULL, 'N' },
	{ "priv-domid", 1, NULL, 'p' },
	{ "output-pid", 0, NULL, 'P' },
	{ "entry-size", 1, NULL, 'S' },
	{ "trace-file", 1, NULL, 'T' },
	{ "transaction", 1, NULL, 't' },
	{ "no-recovery", 0, NULL, 'R' },
	{ "preserve-local", 0, NULL, 'L' },
	{ "internal-db", 0, NULL, 'I' },
	{ "verbose", 0, NULL, 'V' },
	{ "watch-nb", 1, NULL, 'W' },
	{ NULL, 0, NULL, 0 } };

extern void dump_conn(struct connection *conn); 
int dom0_event = 0;
int priv_domid = 0;

int main(int argc, char *argv[])
{
	int opt, *sock, *ro_sock, max;
	fd_set inset, outset;
	bool dofork = true;
	bool outputpid = false;
	bool no_domain_init = false;
	const char *pidfile = NULL;
	int evtchn_fd = -1;
	struct timeval *timeout;

	while ((opt = getopt_long(argc, argv, "DE:F:HNPS:t:T:RLVW:", options,
				  NULL)) != -1) {
		switch (opt) {
		case 'D':
			no_domain_init = true;
			break;
		case 'E':
			quota_nb_entry_per_domain = strtol(optarg, NULL, 10);
			break;
		case 'F':
			pidfile = optarg;
			break;
		case 'H':
			usage();
			return 0;
		case 'N':
			dofork = false;
			break;
		case 'P':
			outputpid = true;
			break;
		case 'R':
			recovery = false;
			break;
		case 'L':
			remove_local = false;
			break;
		case 'S':
			quota_max_entry_size = strtol(optarg, NULL, 10);
			break;
		case 't':
			quota_max_transaction = strtol(optarg, NULL, 10);
			break;
		case 'T':
			tracefile = optarg;
			break;
		case 'I':
			tdb_flags = TDB_INTERNAL|TDB_NOLOCK;
			break;
		case 'V':
			verbose = true;
			break;
		case 'W':
			quota_nb_watch_per_domain = strtol(optarg, NULL, 10);
			break;
		case 'e':
			dom0_event = strtol(optarg, NULL, 10);
			break;
		case 'p':
			priv_domid = strtol(optarg, NULL, 10);
			break;
		}
	}
	if (optind != argc)
		barf("%s: No arguments desired", argv[0]);

	reopen_log();

	/* make sure xenstored directories exist */
	/* Errors ignored here, will be reported when we open files */
	mkdir(xs_daemon_rundir(), 0755);
	mkdir(xs_daemon_rootdir(), 0755);

	if (dofork) {
		openlog("xenstored", 0, LOG_DAEMON);
		daemonize();
	}
	if (pidfile)
		write_pidfile(pidfile);

	/* Talloc leak reports go to stderr, which is closed if we fork. */
	if (!dofork)
		talloc_enable_leak_report_full();

	/* Don't kill us with SIGPIPE. */
	signal(SIGPIPE, SIG_IGN);

	init_sockets(&sock, &ro_sock);
	init_pipe(reopen_log_pipe);

	/* Setup the database */
	setup_structure();

	/* Listen to hypervisor. */
	if (!no_domain_init)
		domain_init();

	/* Restore existing connections. */
	restore_existing_connections();

	if (outputpid) {
		printf("%ld\n", (long)getpid());
		fflush(stdout);
	}

	/* redirect to /dev/null now we're ready to accept connections */
	if (dofork)
		finish_daemonize();

	signal(SIGHUP, trigger_reopen_log);

	if (xce_handle != NULL)
		evtchn_fd = xc_evtchn_fd(xce_handle);

	/* Get ready to listen to the tools. */
	max = initialize_set(&inset, &outset, *sock, *ro_sock, &timeout);

	/* Tell the kernel we're up and running. */
	xenbus_notify_running();

	/* Main loop. */
	for (;;) {
		struct connection *conn, *next;

		if (select(max+1, &inset, &outset, NULL, timeout) < 0) {
			if (errno == EINTR)
				continue;
			barf_perror("Select failed");
		}

		if (reopen_log_pipe[0] != -1 && FD_ISSET(reopen_log_pipe[0], &inset)) {
			char c;
			if (read(reopen_log_pipe[0], &c, 1) != 1)
				barf_perror("read failed");
			reopen_log();
		}

		if (*sock != -1 && FD_ISSET(*sock, &inset))
			accept_connection(*sock, true);

		if (*ro_sock != -1 && FD_ISSET(*ro_sock, &inset))
			accept_connection(*ro_sock, false);

		if (evtchn_fd != -1 && FD_ISSET(evtchn_fd, &inset))
			handle_event();

		next = list_entry(connections.next, typeof(*conn), list);
		if (&next->list != &connections)
			talloc_increase_ref_count(next);
		while (&next->list != &connections) {
			conn = next;

			next = list_entry(conn->list.next,
					  typeof(*conn), list);
			if (&next->list != &connections)
				talloc_increase_ref_count(next);

			if (conn->domain) {
				if (domain_can_read(conn))
					handle_input(conn);
				if (talloc_free(conn) == 0)
					continue;

				talloc_increase_ref_count(conn);
				if (domain_can_write(conn) &&
				    !list_empty(&conn->out_list))
					handle_output(conn);
				if (talloc_free(conn) == 0)
					continue;
			} else {
				if (FD_ISSET(conn->fd, &inset))
					handle_input(conn);
				if (talloc_free(conn) == 0)
					continue;

				talloc_increase_ref_count(conn);
				if (FD_ISSET(conn->fd, &outset))
					handle_output(conn);
				if (talloc_free(conn) == 0)
					continue;
			}
		}

		max = initialize_set(&inset, &outset, *sock, *ro_sock,
				     &timeout);
	}
}

/*
 * Local variables:
 *  c-file-style: "linux"
 *  indent-tabs-mode: t
 *  c-indent-level: 8
 *  c-basic-offset: 8
 *  tab-width: 8
 * End:
 */
