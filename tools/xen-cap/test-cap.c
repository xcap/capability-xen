/**
 * Test API's for xen-cap
 *
 * Authors: Yathindra Naik, ydev@cs.utah.edu
 *
 * This is a simple test file for xen-cap.
 * This will be updated as I make progress.
 *
 * Thie is similar to the code from /xen-tt/tools/ttd/ttd-logd.c
 */

#include <stdio.h>
#include <xenctrl.h>
#include <stdlib.h>

int test_xen_cap_create(void)
{
	uint32_t domain_id = 2;
	int ret;
	xc_interface *xch;
	struct capability *cap = NULL;

	xch = xc_interface_open(0,0,0);
	
	if(xch == 0)
	{
		printf("\nxc_interface_open failed to get xc_handle!\n");
		return 0;
	}
	
	ret = xc_cap_create(xch, cap);
	printf("\nApplication test_cap_create:Domain %d, captype:0x%x\n", domain_id, cap->magic);
	
	return ret;

}

int main(int argc, char** argv)
{
	int ret;

	ret = test_xen_cap_create();

	return ret;
}

