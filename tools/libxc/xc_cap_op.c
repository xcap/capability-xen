/*
 * xc_cap_op.c
 * xc interface to capability operations
 */

#include "xc_private.h"

int xc_cap_create(xc_interface *xch, /*uint32_t domid,*/ struct capability *cap)
{
    DECLARE_CAP_OP;
	int ret;
   	DPRINTF("xc_cap_op: xc_cap_create().\n"); 
	cap_op.cmd 				= XEN_CAP_OP_cap_create;
	/*cap_op.domain = domid;*/
    cap_op.u.cap_create.cap = cap;
	ret = do_cap_op(xch, &cap_op);
	if (!ret)
		cap = cap_op.u.cap_create.cap;
	return ret;
}

int xc_cap_grant(xc_interface *xch, 
		/*uint32_t from_domid,*/ 
		uint32_t to_domid, 
		struct capability *list, 
		int size)
{
    DECLARE_CAP_OP;
	int ret;
	int i;

	DECLARE_HYPERCALL_BUFFER(struct capability, arg);
	DPRINTF("xc_cap_op: xc_cap_grant().\n");	

	arg = xc_hypercall_buffer_alloc(xch, arg, sizeof(*arg)*size);
    if ( arg == NULL )
    {
        PERROR("Could not allocate memory for xc_get_caps_hypercall hypercall");
        return 1;
    }

	for (i=0;i<size;++i)
	{
		if (&list[i] == (struct capability *)NULL)
		{
			PERROR("Could not allocate memory for xc_get_caps_hypercall hypercall");
			return 1;
		}
	}

	cap_op.cmd 				= XEN_CAP_OP_cap_grant;
	/*cap_op.domain 			= (domid_t)from_domid;*/
	cap_op.u.cap_grant.to 	= (domid_t)to_domid;
	cap_op.u.cap_grant.size = size;
	cap_op.u.cap_grant.list  = (struct capability *)HYPERCALL_BUFFER_AS_ARG(arg);

	for (i = 0; i < size; ++i)
	{
		DPRINTF("xc_cap_grant: list[%d] 0x%x addr %p\n",i, list[i].magic, (void*)&list[i]);
		arg[i] = list[i];
	}

//    set_xen_guest_handle(cap_op.u.cap_grant.list, list);

	ret = do_cap_op(xch, &cap_op);
//	xc_hypercall_bounce_post(xch, list);
	xc_hypercall_buffer_free(xch, arg);
	return ret;
}

int xc_cap_check(xc_interface *xch, 
		uint32_t domid, 
		struct capability *cap)
{
    DECLARE_CAP_OP;
   	DPRINTF("xc_cap_op: xc_cap_check().\n"); 
	if (cap == NULL)
		return 0;
	cap_op.cmd 					= XEN_CAP_OP_cap_check;
    cap_op.domain 				= (domid_t)domid;
    cap_op.u.cap_check.cap 		= cap;
    return do_cap_op(xch, &cap_op);
}

void xc_get_caps_hypercall(xc_interface *xch,
		char *tok,
		struct capability *cap)
{
	DECLARE_CAP_OP;
	int ret;
//  DECLARE_HYPERCALL_BOUNCE(cap, sizeof(struct capability), XC_HYPERCALL_BUFFER_BOUNCE_IN);
//	DECLARE_HYPERCALL_BUFFER(struct capability, arg);
//	arg = xc_hypercall_buffer_alloc(xch, arg, sizeof(*arg));
//	if ( arg == NULL )	
//	{
//		PERROR("Could not allocate memory for xc_get_caps_hypercall hypercall");
//		return;
//	}

	DPRINTF("xc_cap_op: xc_get_caps_hypercall sent capability 0x%x addr %p\n",cap->magic,cap);
	cap_op.cmd			= XEN_CAP_OP_get_caps_hypercall;
	//cap_op.domain		= (domid_t)domain;
	cap_op.u.get_caps_hypercall.tok = tok;
	cap_op.u.get_caps_hypercall.cap = cap;//(struct capability *)HYPERCALL_BUFFER_AS_ARG(arg);
//	arg = cap;

	//set_xen_guest_handle(cap_op.u.get_caps_hypercall.cap, cap);
	ret = do_cap_op(xch, &cap_op);
	//cap = cap_op.u.get_caps_hypercall.cap;	
    DPRINTF("xc_cap_op: xc_get_caps_hypercall rcvd capability 0x%x addr %p\n",cap->magic, (void*)cap);
	//xc_hypercall_bounce_post(xch, cap);
	//xc_hypercall_buffer_free(xch, arg);
}

